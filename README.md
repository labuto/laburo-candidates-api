## postgres extension:
  create extension if not exists "postgis";
  create extension if not exists "fuzzystrmatch";
  create extension if not exists "postgis_tiger_geocoder";
  create extension cube;
  create extension earthdistance;

## 18.196.228.81

npm cache clean --force
rm -rf ~/.npm
# In the project folder:
rm -rf node_modules
rm -f package-lock.json