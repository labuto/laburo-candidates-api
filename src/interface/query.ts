export interface QueryInterface {
    text: string,
    values: Array<any>
}