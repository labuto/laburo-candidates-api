import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    BaseEntity,
    JoinColumn,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne, Unique,
} from 'typeorm'

import typeOrm from '../services/typeOrm'
import { Job } from './Job'
import { Company } from './Company'
import { Candidate } from './Candidate/Candidate';
import {CandidateApproveManger, CandidateAssignment, CandidateQuizzes} from './Candidate'
import { CandidateSendDocuments } from './Candidate/CandidateSendDocuments';

@Unique(['candidate_id', 'job_id'])
@Entity({ name: 'job_process' })
export class JobAdvertise extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @ManyToOne(() => Job, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'job_id' })
    job: Job;

    @ManyToOne(() => Company, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'company_id' })
    company: Company;

    @ManyToOne(() => Candidate, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'candidate_id' })
    candidate: Candidate;

    @Column({ nullable: true, type: 'bigint' })
    job_id: number;

    @Column({ nullable: true, type: 'bigint' })
    company_id: number;

    @Column({ nullable: true, type: 'bigint' })
    candidate_id: number;

    @Column({ nullable: true, length: 55 })
    status: string

    @Column({ nullable: true, type: 'float' })
    percentage: number

    @Column({ nullable: true, type: 'json' })
    percentage_partition: object

    @Column({ nullable: true, default: 0 })
    phase: number

    @Column({ nullable: true, type: 'float' })
    current_weight: number

    @ManyToOne(() => CandidateQuizzes, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'candidate_quiz_id' })
    candidate_quiz: CandidateQuizzes;

    @Column({ nullable: true, type: 'bigint' })
    candidate_quiz_id: number;

    @ManyToOne(() => CandidateAssignment, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'candidate_assignment_id' })
    candidate_assignment: CandidateAssignment;

    @Column({ nullable: true, type: 'bigint' })
    candidate_assignment_id: number;

    @ManyToOne(() => CandidateApproveManger, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'candidate_approve_manager_id' })
    candidate_approve_manager: CandidateApproveManger;

    @Column({ nullable: true, type: 'bigint' })
    candidate_approve_manager_id: number;

    @ManyToOne(() => CandidateSendDocuments, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'candidate_send_documents_id' })
    candidate_send_documents: CandidateSendDocuments;

    @Column({ nullable: true, type: 'bigint' })
    candidate_send_documents_id: number;

    @Column({ nullable: true, type: 'json' })
    previous: object;

    @Column({ nullable: true, type: 'bigint' })
    created_date: number;

    @Column({ nullable: true, type: 'bigint' })
    updated_date: number;

    @Column({ nullable: true, type: 'bigint' })
    finish_date: number;

    @Column({ nullable: true, default: true })
    is_removed: boolean;
}
