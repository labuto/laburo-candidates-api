import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, JoinColumn, CreateDateColumn, UpdateDateColumn, ManyToOne, PrimaryColumn} from 'typeorm'
import {User} from '../User'
import {Candidate} from '../Candidate'
import {Comment} from './Comment'

@Entity({ name: 'comment_not_like' })
export class CommentNotLike extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @ManyToOne(() => Comment, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'comment_id' })
    comment: Comment;

    @ManyToOne(() => User, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'user_id' })
    user: User;

    @ManyToOne(() => Candidate, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'candidate_id' })
    candidate: Candidate;

    @Column({ nullable: true, type: 'bigint' })
    comment_id: number;

    @Column({ nullable: true, type: 'bigint' })
    user_id: number;

    @Column({ nullable: true, type: 'bigint' })
    candidate_id: number;

    @Column({ nullable: true, default: false })
    is_laburo: boolean;

    @CreateDateColumn({ type: 'timestamp' })
    created_at: Date;
}
