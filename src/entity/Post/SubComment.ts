import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, JoinColumn, CreateDateColumn, UpdateDateColumn, ManyToOne, PrimaryColumn} from 'typeorm'
import typeOrm from '../../services/typeOrm'
import {User} from '../User'
import {Candidate} from '../Candidate'
import {Comment} from './Comment'

@Entity({ name: 'sub_comments' })
export class SubComment extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @ManyToOne(() => Comment, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'comment_id' })
    comment: Comment;

    @ManyToOne(() => User, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'user_id' })
    user: User;

    @ManyToOne(() => Candidate, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'candidate_id' })
    candidate: Candidate;

    @Column({ nullable: true, type: 'bigint' })
    comment_id: number;

    @Column({ nullable: true, type: 'bigint' })
    user_id: number;

    @Column({ nullable: true, type: 'bigint' })
    candidate_id: number;

    @Column({ nullable: true, default: false })
    is_laburo: boolean;

    @Column({ nullable: true, type: 'text' })
    content: string;

    @Column({ nullable: true })
    filling: string;

    @Column({ nullable: true })
    privacy: string;

    @CreateDateColumn({ type: 'timestamp' })
    created_at: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    updated_at: Date;

    @Column({ type: 'timestamp', nullable: true })
    deleted_at: Date;

    static async softRemoveMany (ids: Array<number>) {
        const query = {
            text: `UPDATE sub_comments SET deleted_at = CURRENT_TIMESTAMP WHERE id IN(${ids.map((id, i) => `$${i + 1}`)})`,
            values: ids
        }
        await typeOrm.q(query.text, query.values)
    }
}
