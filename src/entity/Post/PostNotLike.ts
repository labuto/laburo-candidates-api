import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, JoinColumn, CreateDateColumn, UpdateDateColumn, ManyToOne, PrimaryColumn} from 'typeorm'
import typeOrm from '../../services/typeOrm'
import {User} from '../User'
import {Candidate} from '../Candidate'
import {Post} from './Post'

@Entity({ name: 'post_not_like' })
export class PostNotLike extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @ManyToOne(() => Post, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'post_id' })
    post: Post;

    @ManyToOne(() => User, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'user_id' })
    user: User;

    @ManyToOne(() => Candidate, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'candidate_id' })
    candidate: Candidate;

    @Column({ nullable: true, type: 'bigint' })
    post_id: number;

    @Column({ nullable: true, type: 'bigint' })
    user_id: number;

    @Column({ nullable: true, type: 'bigint' })
    candidate_id: number;

    @Column({ nullable: true, default: false })
    is_laburo: boolean;

    @CreateDateColumn({ type: 'timestamp' })
    created_at: Date;
}
