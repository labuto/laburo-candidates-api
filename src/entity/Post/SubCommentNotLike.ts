import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, JoinColumn, CreateDateColumn, UpdateDateColumn, ManyToOne, PrimaryColumn} from 'typeorm'
import {User} from '../User'
import {Candidate} from '../Candidate'
import {SubComment} from './SubComment'

@Entity({ name: 'sub_comment_not_like' })
export class SubCommentNotLike extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @ManyToOne(() => SubComment, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'sub_comment_id' })
    sub_comment: SubComment;

    @ManyToOne(() => User, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'user_id' })
    user: User;

    @ManyToOne(() => Candidate, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'candidate_id' })
    candidate: Candidate;

    @Column({ nullable: true, type: 'bigint' })
    sub_comment_id: number;

    @Column({ nullable: true, type: 'bigint' })
    user_id: number;

    @Column({ nullable: true, type: 'bigint' })
    candidate_id: number;

    @Column({ nullable: true, default: false })
    is_laburo: boolean;

    @CreateDateColumn({ type: 'timestamp' })
    created_at: Date;
}
