import {
    Entity, PrimaryGeneratedColumn, Column, BaseEntity, JoinColumn, CreateDateColumn, UpdateDateColumn,
    ManyToOne, PrimaryColumn
} from 'typeorm'

import typeOrm from '../services/typeOrm'
import { Company } from './Company'
import { User } from './User'

@Entity({ name: 'emails' })
export class Email extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @ManyToOne(() => Company, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'company_id' })
    company: Company;

    @ManyToOne(() => User, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'created_id' })
    user: User;

    @Column({ nullable: true, type: 'bigint' })
    company_id: number;

    @Column({ nullable: true, type: 'bigint' })
    created_id: number;

    @Column({ nullable: true })
    type: string;

    @Column({ nullable: true, length: 255 })
    subject: string;

    @Column({ nullable: true, type: 'text' })
    content: string;

    @CreateDateColumn({ type: 'timestamp' })
    created_at: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    updated_at: Date;

    @Column({ type: 'timestamp', nullable: true })
    deleted_at: Date;

    static async softRemoveMany (ids: Array<number>) {
        const query = {
            text: `UPDATE leads SET deleted_at = CURRENT_TIMESTAMP WHERE id IN(${ids.map((id, i) => `$${i + 1}`)})`,
            values: ids
        }
        await typeOrm.q(query.text, query.values)
    }

    async softRemove (): Promise<void> {
        console.log(this)
        console.log('here')
    }
}
