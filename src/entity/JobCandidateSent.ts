import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    BaseEntity,
    JoinColumn,
    CreateDateColumn,
    ManyToOne
} from 'typeorm'

import { Company } from './Company'
import { Job } from './Job'
import {Candidate} from './Candidate'

@Entity({ name: 'job_candidate_sent' })
export class JobCandidateSent extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @ManyToOne(() => Job, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'job_id' })
    job: Job;

    @ManyToOne(() => Company, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'company_id' })
    company: Company;

    @ManyToOne(() => Candidate, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'candidate_id' })
    candidate: Candidate;

    @Column({ nullable: true, type: 'bigint' })
    job_id: number;

    @Column({ nullable: true, type: 'bigint' })
    company_id: number;

    @Column({ nullable: true, type: 'bigint' })
    candidate_id: number;

    @CreateDateColumn({ type: 'timestamp' })
    created_at: Date;
}
