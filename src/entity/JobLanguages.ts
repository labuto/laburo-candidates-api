import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    BaseEntity,
    JoinColumn,
    ManyToOne,
} from 'typeorm'

import { Company } from './Company'
import { Job } from './Job'

@Entity({ name: 'job_languages' })
export class JobLanguages extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @ManyToOne(() => Job, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'job_id' })
    job: Job;

    @ManyToOne(() => Company, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'company_id' })
    company: Company;

    @Column({ nullable: true, type: 'bigint' })
    job_id: number;

    @Column({ nullable: true, type: 'bigint' })
    company_id: number;

    @Column({ nullable: true, length: 50 })
    key: string;

    @Column({ nullable: true, type: 'int' })
    value: number;

    @Column({ nullable: true, type: 'int' })
    rank: number;
}
