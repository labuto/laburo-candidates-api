import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    BaseEntity,
    JoinColumn,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne,
    PrimaryColumn
} from 'typeorm'
import typeOrm from '../../services/typeOrm'

@Entity({ name: 'similar_position_match' })
export class SimilarPosition extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @Column({ nullable: true })
    similar_field: string;

    @Column({ nullable: true })
    similar_position: string;

    @Column({ nullable: true })
    field: string;

    @Column({ nullable: true })
    position: string;

    @Column({ nullable: true })
    counter: number;

    @CreateDateColumn({ type: 'timestamp' })
    created_at: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    updated_at: Date;

    @Column({ type: 'timestamp', nullable: true })
    deleted_at: Date;

    static async softRemoveMany (ids: Array<number>) {
        const query = {
            text: `UPDATE SimilarPosition SET deleted_at = CURRENT_TIMESTAMP WHERE id IN(${ids.map((id, i) => `$${i + 1}`)})`,
            values: ids
        }
        await typeOrm.q(query.text, query.values)
    }
}
