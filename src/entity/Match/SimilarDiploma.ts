import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    BaseEntity,
    JoinColumn,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne,
    PrimaryColumn
} from 'typeorm'
import typeOrm from '../../services/typeOrm'

@Entity({ name: 'similar_diploma_match' })
export class SimilarDiploma extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @Column({ nullable: true })
    similar_field: string;

    @Column({ nullable: true })
    similar_diploma: string;

    @Column({ nullable: true })
    field: string;

    @Column({ nullable: true })
    diploma: string;

    @Column({ nullable: true })
    counter: number;

    @CreateDateColumn({ type: 'timestamp' })
    created_at: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    updated_at: Date;

    @Column({ type: 'timestamp', nullable: true })
    deleted_at: Date;

    static async softRemoveMany (ids: Array<number>) {
        const query = {
            text: `UPDATE similarDiplomaMatch SET deleted_at = CURRENT_TIMESTAMP WHERE id IN(${ids.map((id, i) => `$${i + 1}`)})`,
            values: ids
        }
        await typeOrm.q(query.text, query.values)
    }
}
