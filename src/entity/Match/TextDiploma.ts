import {
    Entity, PrimaryGeneratedColumn, Column, BaseEntity, PrimaryColumn, CreateDateColumn, UpdateDateColumn
} from 'typeorm'
import typeOrm from '../../services/typeOrm'

@Entity({ name: 'text_diploma_match' })
export class TextDiploma extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @Column({ nullable: true, type: 'text' })
    text: string;

    @Column({ nullable: true })
    field: string;

    @Column({ nullable: true })
    diploma: string;

    @Column({ nullable: true })
    counter: number;

    @CreateDateColumn({ type: 'timestamp' })
    created_at: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    updated_at: Date;

    @Column({ type: 'timestamp', nullable: true })
    deleted_at: Date;

    static async softRemoveMany (ids: Array<number>) {
        const query = {
            text: `UPDATE textDiplomaMatch SET deleted_at = CURRENT_TIMESTAMP WHERE id IN(${ids.map((id, i) => `$${i + 1}`)})`,
            values: ids
        }
        await typeOrm.q(query.text, query.values)
    }
}
