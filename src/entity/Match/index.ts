import { SimilarDiploma } from './SimilarDiploma'
import { SimilarMilitaryProfession } from './SimilarMilitaryProfession'
import { SimilarPosition } from './SimilarPosition'
import { TextDiploma } from './TextDiploma'
import { TextMilitaryProfession } from './TextMilitaryProfession'
import { TextPosition } from './TextPosition'

export {
    SimilarDiploma,
    SimilarMilitaryProfession,
    SimilarPosition,
    TextDiploma,
    TextMilitaryProfession,
    TextPosition
}
