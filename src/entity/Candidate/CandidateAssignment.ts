import {
    Entity, PrimaryGeneratedColumn, Column, BaseEntity, JoinColumn, CreateDateColumn,
    ManyToOne, OneToOne, Index, PrimaryColumn
} from 'typeorm'

import { Job } from '../Job'
import { Assignment } from '../Assignment'
import {Candidate} from './Candidate'

@Index(['candidate_id', 'job_id', 'assignment_id'], { unique: true })
@Entity({ name: 'candidate_assignment' })
export class CandidateAssignment extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @ManyToOne(() => Candidate, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'candidate_id' })
    candidate: Candidate;

    @ManyToOne(() => Job, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'job_id' })
    job: Job;

    @OneToOne(() => Assignment, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'assignment_id' })
    assignment: Assignment;

    @Column({ nullable: true, type: 'bigint' })
    candidate_id: number;

    @Column({ nullable: true, type: 'bigint' })
    job_id: number;

    @Column({ nullable: true, type: 'bigint' })
    assignment_id: number;

    @Column({ nullable: true })
    grade: number;

    @CreateDateColumn({ type: 'timestamp' })
    created_date: Date;

    @Column({ type: 'timestamp' })
    finished_date: Date;
}
