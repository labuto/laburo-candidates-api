import {
    Entity, PrimaryGeneratedColumn, Column, BaseEntity, JoinColumn, OneToOne, PrimaryColumn, ManyToOne
} from 'typeorm'

import { Candidate } from './Candidate'

@Entity({ name: 'candidate_education' })
export class CandidateEducation extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @ManyToOne(() => Candidate, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'candidate_id' })
    candidate: Candidate;

    @Column({ nullable: true, type: 'bigint' })
    candidate_id: number;

    @Column({ nullable: true })
    diploma_type: string;

    @Column({ nullable: true, type: 'json' })
    diplomas: Array<any>;

    @Column({ nullable: true })
    institute: string;

    @Column({ nullable: true })
    from_date: number;

    @Column({ nullable: true })
    to_date: number;

    @Column({ nullable: true, type: 'text' })
    description: string;

    @Column({ nullable: true, type: 'float' })
    grade: number;

    @Column({ nullable: true, type: 'json' })
    comparison_obj: object;

}
