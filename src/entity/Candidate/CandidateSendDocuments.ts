import {
    Entity, PrimaryGeneratedColumn, Column, BaseEntity, JoinColumn, CreateDateColumn, ManyToOne, OneToOne, PrimaryColumn
} from 'typeorm'

import { Candidate } from './Candidate'
import { Job } from '../Job'

@Entity({ name: 'candidate_send_documents' })
export class CandidateSendDocuments extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @Column({ nullable: true })
    username: string;

    @ManyToOne(() => Candidate, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'candidate_id' })
    candidate: Candidate;

    @ManyToOne(() => Job, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'job_id' })
    job: Job;

    @Column({ nullable: true, type: 'bigint' })
    candidate_id: number;

    @Column({ nullable: true, type: 'bigint' })
    job_id: number;

    @Column({ type: 'json', nullable: true })
    attachments: object;

    @CreateDateColumn({ type: 'timestamp' })
    created_at: Date;

    @Column({ type: 'timestamp' })
    finished_at: Date;

}
