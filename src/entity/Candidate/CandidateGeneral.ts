import {
    Entity, PrimaryGeneratedColumn, Column, BaseEntity, JoinColumn, OneToOne, PrimaryColumn, ManyToOne
} from 'typeorm'

import {Candidate} from './Candidate'

@Entity({ name: 'candidate_general' })
export class CandidateGeneral extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @ManyToOne(() => Candidate, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'candidate_id' })
    candidate: Candidate;

    @Column({ nullable: true, type: 'bigint' })
    candidate_id: number;

    @Column({ nullable: true, length: 255 })
    resume: string;

    @Column({ nullable: true, length: 255 })
    photo: string;

    @Column({ nullable: true })
    phone: string;

    @Column({ nullable: true, type: 'timestamp' })
    birthday: Date;

    @Column({ nullable: true })
    gender: string;

    @Column({ nullable: true, type: 'json' })
    location: object;

    @Column({ nullable: true })
    family_status: string;

    @Column({ nullable: true, type: 'text' })
    about_me: string;

    @Column({ nullable: true })
    facebook: string;

    @Column({ nullable: true })
    linkedin: string;

    @Column({ nullable: true, type: 'text' })
    notes: string;

    @Column({ nullable: true, type: 'json' })
    extra_files: Array<any>;
}
