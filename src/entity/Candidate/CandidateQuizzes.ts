import {
    Entity, PrimaryGeneratedColumn, Column, BaseEntity, JoinColumn, CreateDateColumn, ManyToOne, OneToOne, PrimaryColumn
} from 'typeorm'

import { Job } from '../Job'
import { Candidate } from './Candidate'
import { Assignment } from '../Assignment'
import { Quiz } from '../Quiz'

@Entity({ name: 'candidate_quizzes' })
export class CandidateQuizzes extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @ManyToOne(() => Candidate, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'candidate_id' })
    candidate: Candidate;

    @ManyToOne(() => Job, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'job_id' })
    job: Job;

    @OneToOne(() => Quiz, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'quiz_id' })
    quiz: Quiz;

    @OneToOne(() => Assignment, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'assignment_id' })
    assignment: Assignment;

    @Column({ nullable: true, type: 'bigint' })
    candidate_id: number;

    @Column({ nullable: true, type: 'bigint' })
    job_id: number;

    @Column({ nullable: true, type: 'bigint' })
    quiz_id: number;

    @Column({ nullable: true, type: 'bigint' })
    assignment_id: number;

    @Column({ nullable: true, type: 'float' })
    grade: number;

    @Column({ nullable: true, type: 'timestamp' })
    due_date: Date;

    @Column({ nullable: true, type: 'float' })
    duration: number;

    @Column({ nullable: true, type: 'timestamp' })
    start_date: Date;

    @Column({ nullable: true, type: 'timestamp' })
    end_date: Date;

    @Column({ nullable: true, type: 'text' })
    answer: string;

    @CreateDateColumn({ type: 'timestamp' })
    created_date: Date;

    @Column({ type: 'timestamp' })
    finished_date: Date;
}
