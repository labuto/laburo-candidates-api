import {
    Entity, PrimaryGeneratedColumn, Column, BaseEntity, JoinColumn, OneToOne, PrimaryColumn, ManyToOne
} from 'typeorm'

import {Candidate} from './Candidate'

@Entity({ name: 'candidate_job_search' })
export class CandidateJobSearch extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @ManyToOne(() => Candidate, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'candidate_id' })
    candidate: Candidate;

    @Column({ nullable: true, type: 'bigint' })
    candidate_id: number;

    @Column({ nullable: true, type: 'json' })
    position: object;

    @Column({ nullable: true, type: 'json' })
    company_types: Array<any>;

    @Column({ nullable: true, type: 'json' })
    location: object;

    @Column({ nullable: true, type: 'json' })
    job_scope: Array<any>;

    @Column({ nullable: true, type: 'json' })
    work_availability: Array<any>;

    @Column({ nullable: true, type: 'float' })
    salary: number;

    @Column({ nullable: true, type: 'text' })
    description: string;
}
