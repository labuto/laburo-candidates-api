import {
    Entity, PrimaryGeneratedColumn, Column, BaseEntity, JoinColumn, OneToOne, PrimaryColumn, ManyToOne
} from 'typeorm'

import {Candidate} from './Candidate'

@Entity({ name: 'candidate_extra_files' })
export class CandidateExtraFile extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @ManyToOne(() => Candidate, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'candidate_id' })
    candidate: Candidate;

    @Column({ nullable: true, type: 'bigint' })
    candidate_id: number;

    @Column({ nullable: true })
    file_name: string;

    @Column({ nullable: true })
    file_link: string;
}