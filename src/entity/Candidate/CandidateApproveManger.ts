import {
    Entity, PrimaryGeneratedColumn, Column, BaseEntity, JoinColumn, CreateDateColumn, ManyToOne, OneToOne, PrimaryColumn
} from 'typeorm'

import { Candidate } from './Candidate'
import { Job } from '../Job'

@Entity({ name: 'candidate_approve_manager' })
export class CandidateApproveManger extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @Column({ nullable: true })
    username: string;

    @ManyToOne(() => Candidate, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'candidate_id' })
    candidate: Candidate;

    @ManyToOne(() => Job, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'job_id' })
    job: Job;

    @Column({ nullable: true, type: 'bigint' })
    candidate_id: number;

    @Column({ nullable: true, type: 'bigint' })
    job_id: number;

    @Column({ type: 'json', nullable: true })
    assignments: Array<number>;

    @Column({ nullable: true, default: false })
    liked: boolean;

    @CreateDateColumn({ type: 'timestamp' })
    created_date: Date;

    @Column({ type: 'timestamp' })
    finished_date: Date;

}
