import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    BaseEntity,
    JoinColumn,
    CreateDateColumn,
    UpdateDateColumn,
    PrimaryColumn, ManyToOne, OneToOne
} from 'typeorm'
import typeOrm from '../../services/typeOrm'
import {Job} from '../Job'
import {CandidateGeneral} from './CandidateGeneral'

@Entity({ name: 'candidates' })
export class Candidate extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @OneToOne(() => CandidateGeneral)
    general: CandidateGeneral;

    @Column({ nullable: true })
    full_name: string;

    @Column({ nullable: true })
    hash: string;

    @Column({ nullable: true })
    email: string;

    @Column({ type: 'json', nullable: true })
    companies: Array<any>;

    @Column({ type: 'json', nullable: true })
    not_companies: Array<any>;

    @Column({ nullable: true, default: 'FALSE' })
    approved: boolean;

    @Column({ nullable: true, default: 'private' })
    privacy: string;

    @Column({ nullable: true })
    source: string;

    @Column({ nullable: true, default: 'TRUE' })
    first_login: boolean;

    @Column({ nullable: true, default: 'TRUE' })
    is_active: boolean;

    @CreateDateColumn({ type: 'timestamp' })
    created_date: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    updated_date: Date;

    @Column({ nullable: true, default: false })
    is_deleted: boolean;

    @Column({ type: 'timestamp', nullable: true })
    deleted_date: Date;

    static async softRemoveMany (ids: Array<number>) {
        const query = {
            text: `UPDATE candidates SET deleted_date = CURRENT_TIMESTAMP WHERE id IN(${ids.map((id, i) => `$${i + 1}`)})`,
            values: ids
        }
        await typeOrm.q(query.text, query.values)
    }
}
