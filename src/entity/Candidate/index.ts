import { Candidate } from './Candidate'
import { CandidateLanguages } from './CandidateLanguages'
import { CandidateJobSearch } from './CandidateJobSearch'
import { CandidateExperience } from './CandidateExperience'
import { CandidateEducation } from './CandidateEducation'
import { CandidateMilitary } from './CandidateMilitary'
import { CandidateQuizzes } from './CandidateQuizzes'
import { CandidateAssignment } from './CandidateAssignment'
import { CandidateNotes } from './CandidateNotes'
import { CandidateApproveManger } from './CandidateApproveManger'
import { CandidateKey } from './CandidateKey'
import { CandidateGeneral } from './CandidateGeneral'
import { CandidateLogs } from './CandidateLogs'
import { CandidateJobShare } from './CandidateJobShare'
import { CandidateUnreadResume } from './CandidateUnreadResume'

export {
    Candidate,
    CandidateLanguages,
    CandidateJobSearch,
    CandidateExperience,
    CandidateEducation,
    CandidateMilitary,
    CandidateQuizzes,
    CandidateAssignment,
    CandidateNotes,
    CandidateApproveManger,
    CandidateKey,
    CandidateGeneral,
    CandidateLogs,
    CandidateJobShare,
    CandidateUnreadResume
}
