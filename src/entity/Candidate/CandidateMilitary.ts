import {
    Entity, PrimaryGeneratedColumn, Column, BaseEntity, JoinColumn, OneToOne, PrimaryColumn, ManyToOne
} from 'typeorm'

import { Candidate } from './Candidate'

@Entity({ name: 'candidate_military' })
export class CandidateMilitary extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @ManyToOne(() => Candidate, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'candidate_id' })
    candidate: Candidate;

    @Column({ nullable: true, type: 'bigint' })
    candidate_id: number;

    @Column({ type: 'json', nullable: true })
    professions: Array<any>;

    @Column({ type: 'json', nullable: true })
    departments: Array<any>;

    @Column({ type: 'json', nullable: true })
    roles: Array<any>;

    @Column({ nullable: true })
    rank: string;

    @Column({ nullable: true })
    unit: string;

    @Column({ nullable: true })
    from_date: number;

    @Column({ nullable: true })
    to_date: number;

    @Column({ nullable: true, type: 'text' })
    description: string;

    @Column({ type: 'json', nullable: true })
    comparison_obj: object;
}
