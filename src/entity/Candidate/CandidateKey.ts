import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    BaseEntity,
    CreateDateColumn,
    UpdateDateColumn,
    PrimaryColumn
} from 'typeorm'

@Entity({ name: 'candidate_key' })
export class CandidateKey extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @Column({ nullable: true, unique: true })
    candidate_email: string;

    @Column({ nullable: true, type: 'text' })
    key: string;

    @CreateDateColumn({ type: 'timestamp' })
    created_date: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    updated_date: Date;
}
