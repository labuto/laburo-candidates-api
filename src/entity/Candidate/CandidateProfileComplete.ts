import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    BaseEntity,
    JoinColumn,
    OneToOne,
    PrimaryColumn,
    ManyToOne,
    CreateDateColumn,
    UpdateDateColumn
} from 'typeorm'

import { Candidate } from './Candidate'
import {Job} from '../index'

@Entity({ name: 'candidate_profile_complete' })
export class CandidateProfileComplete extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @ManyToOne(() => Candidate, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'candidate_id' })
    candidate: Candidate;

    @Column({ nullable: true, type: 'bigint' })
    candidate_id: number;

    @Column({ nullable: true, default: 'FALSE' })
    general: boolean;

    @Column({ nullable: true, default: 'FALSE' })
    experience: boolean;

    @Column({ nullable: true, default: 'FALSE' })
    education: boolean;

    @Column({ nullable: true, default: 'FALSE' })
    military: boolean;

    @Column({ nullable: true, default: 'FALSE' })
    skills: boolean;
}