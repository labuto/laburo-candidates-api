import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    BaseEntity,
    JoinColumn,
    OneToOne,
    PrimaryColumn,
    ManyToOne,
    CreateDateColumn,
    UpdateDateColumn
} from 'typeorm'

import { Candidate } from './Candidate'
import {Job} from '../index'

@Entity({ name: 'candidate_job_share' })
export class CandidateJobShare extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @ManyToOne(() => Candidate, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'candidate_id' })
    candidate: Candidate;

    @Column({ nullable: true, type: 'bigint' })
    candidate_id: number;

    @ManyToOne(() => Job, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'job_id' })
    job: Job;

    @Column({ nullable: true, type: 'bigint' })
    job_id: number;

    @Column({ nullable: true, length: 255 })
    share_email: string;

    @Column({ nullable: true, default: 'FALSE' })
    approve: boolean;

    @CreateDateColumn({ type: 'timestamp' })
    created_at: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    updated_at: Date;

    @Column({ type: 'timestamp', nullable: true })
    deleted_at: Date;
}