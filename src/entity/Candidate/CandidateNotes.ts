import {
    Entity, PrimaryGeneratedColumn, Column, BaseEntity, JoinColumn, ManyToOne, OneToOne, PrimaryColumn
} from 'typeorm'

import { Job } from '../Job'
import { Candidate } from './Candidate'
import { Company } from '../Company'

@Entity({ name: 'candidate_notes' })
export class CandidateNotes extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @ManyToOne(() => Candidate, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'candidate_id' })
    candidate: Candidate;

    @ManyToOne(() => Company, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'company_id' })
    company: Company;

    @ManyToOne(() => Job, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'job_id' })
    job: Job;

    @Column({ nullable: true, type: 'bigint' })
    candidate_id: number;

    @Column({ nullable: true, type: 'bigint' })
    company_id: number;

    @Column({ nullable: true, type: 'bigint' })
    job_id: number;

    @Column({ nullable: true })
    process_type: string;

    @Column({ nullable: true, type: 'int' })
    phase: number;

    @Column({ nullable: true, type: 'json' })
    notes: Array<any>;

    @Column({ nullable: true })
    is_general: boolean;
}
