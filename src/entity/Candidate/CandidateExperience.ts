import {
    Entity, PrimaryGeneratedColumn, Column, BaseEntity, JoinColumn, OneToOne, PrimaryColumn, ManyToOne
} from 'typeorm'

import {Candidate} from './Candidate'

@Entity({ name: 'candidate_experience' })
export class CandidateExperience extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @ManyToOne(() => Candidate, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'candidate_id' })
    candidate: Candidate;

    @Column({ nullable: true, type: 'bigint' })
    candidate_id: number;

    @Column({ nullable: true, type: 'json' })
    positions: Array<any>;

    @Column({ nullable: true })
    company_type: string;

    @Column({ nullable: true })
    company_name: string;

    @Column({ nullable: true, type: 'json' })
    location: object;

    @Column({ nullable: true, type: 'float' })
    salary: number;

    @Column({ nullable: true })
    job_scope: string;

    @Column({ nullable: true, type: 'text' })
    job_likes: string;

    @Column({ nullable: true, type: 'text' })
    job_dislikes: string;

    @Column({ nullable: true })
    from_date: number;

    @Column({ nullable: true })
    to_date: number;

    @Column({ nullable: true, type: 'text' })
    description: string;

    @Column({ nullable: true, type: 'json' })
    computer_systems: Array<any>;

    @Column({ nullable: true, type: 'json' })
    recommend: object;

    @Column({ nullable: true, type: 'json' })
    comparison_obj: object;
}
