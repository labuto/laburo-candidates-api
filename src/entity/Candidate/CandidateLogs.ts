import {
    Entity, PrimaryGeneratedColumn, Column, BaseEntity, JoinColumn, OneToOne, PrimaryColumn, ManyToOne
} from 'typeorm'

import { Candidate } from './Candidate'

@Entity({ name: 'candidate_logs' })
export class CandidateLogs extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @ManyToOne(() => Candidate, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'candidate_id' })
    candidate: Candidate;

    @Column({ nullable: true, type: 'bigint' })
    candidate_id: number;

    @Column({ nullable: true, type: 'timestamp' })
    enter_date: Date;

    @Column({ nullable: true, type: 'timestamp' })
    exit_date: Date;
}
