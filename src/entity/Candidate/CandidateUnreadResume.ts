import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    BaseEntity,
    CreateDateColumn,
    UpdateDateColumn,
    PrimaryColumn, ManyToOne, JoinColumn
} from 'typeorm'
import {Job} from '../Job'

@Entity({ name: 'candidate_unread_resume' })
export class CandidateUnreadResume extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @ManyToOne(() => Job, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'job_id' })
    job: Job;

    @Column({ nullable: true, type: 'bigint' })
    job_id: number;

    @Column({ nullable: true })
    file_name: string;

    @Column({ nullable: true })
    source: string;

    @Column({ nullable: true, default: false })
    is_read: boolean;

    @CreateDateColumn({ type: 'timestamp' })
    created_at: Date;
}
