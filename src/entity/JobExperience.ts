import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    BaseEntity,
    JoinColumn,
    ManyToOne,
} from 'typeorm'

import { Job } from './Job'
import { Company } from './Company'

@Entity({ name: 'job_experience' })
export class JobExperience extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @ManyToOne(() => Job, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'job_id' })
    job: Job;

    @ManyToOne(() => Company, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'company_id' })
    company: Company;


    @Column({ nullable: true, type: 'bigint' })
    job_id: number;

    @Column({ nullable: true, type: 'bigint' })
    company_id: number;

    @Column({ nullable: true, type: 'json' })
    positions: object

    @Column({ nullable: true, type: 'int' })
    years: number

    @Column({ nullable: true, type: 'int' })
    max_years: number

    @Column({ nullable: true, type: 'int' })
    rank: number
}
