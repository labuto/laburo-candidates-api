import {
    Entity, PrimaryGeneratedColumn, Column, BaseEntity, JoinColumn, CreateDateColumn, UpdateDateColumn,
    ManyToOne, PrimaryColumn
} from 'typeorm'
import typeOrm from '../services/typeOrm'
import {Company} from './Company'

@Entity({ name: 'quizzes' })
export class Quiz extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @ManyToOne(() => Company, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'company_id' })
    company: Company;

    @Column({ nullable: true, type: 'bigint' })
    company_id: number;

    @Column({ nullable: true })
    title: string;

    @Column({ nullable: true, length: 255 })
    link: string;

    @Column({ nullable: true, type: 'json' })
    questions: Array<any>;

    @Column({ nullable: true })
    type: string;

    @Column({ nullable: true, length: 255 })
    file_name: string;

    @CreateDateColumn({ type: 'timestamp' })
    created_at: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    updated_at: Date;

    @Column({ type: 'timestamp', nullable: true })
    deleted_at: Date;

    static async softRemoveMany (ids: Array<number>) {
        const query = {
            text: `UPDATE candidates SET deleted_at = CURRENT_TIMESTAMP WHERE id IN(${ids.map((id, i) => `$${i + 1}`)})`,
            values: ids
        }
        await typeOrm.q(query.text, query.values)
    }
}
