import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, JoinColumn, CreateDateColumn, UpdateDateColumn, ManyToOne, PrimaryColumn} from 'typeorm'
import typeOrm from '../services/typeOrm'
import { User } from './User'
import { Company } from './Company'

@Entity({ name: 'assignments' })
export class Assignment extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @ManyToOne(() => User, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'created_id' })
    user: User;

    @ManyToOne(() => Company, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'company_id' })
    company: Company;

    @Column({ nullable: true, type: 'bigint' })
    created_id: number;

    @Column({ nullable: true, type: 'bigint' })
    company_id: number;

    @Column({ nullable: true })
    title: string;

    @Column({ nullable: true })
    type: string;

    @ManyToOne(() => User, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'assign_id' })
    userAssign: User;

    @Column({ nullable: true, type: 'bigint' })
    assign_id: number;

    @Column({ nullable: true, type: 'text' })
    description: string;

    @Column({ nullable: true, type: 'timestamp' })
    date: Date;

    @Column({ nullable: true })
    year: number;

    @Column({ nullable: true })
    month: number;

    @Column({ nullable: true })
    status: string;

    @Column({ nullable: true, type: 'json' })
    users: Array<any>;

    @Column({ nullable: true, default: 'FALSE' })
    alert: boolean;

    @Column({ nullable: true, type: 'json' })
    location: object;

    @Column({ nullable: true, type: 'json' })
    meeting_summary: object;

    @Column({ nullable: true, default: 'FALSE' })
    is_accepted: boolean;

    @CreateDateColumn({ type: 'timestamp' })
    created_at: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    updated_at: Date;

    @Column({ type: 'timestamp', nullable: true })
    finished_at: Date;

    @Column({ type: 'timestamp', nullable: true })
    deleted_at: Date;

    static async softRemoveMany (ids: Array<number>) {
        const query = {
            text: `UPDATE assignments SET deleted_at = CURRENT_TIMESTAMP WHERE id IN(${ids.map((id, i) => `$${i + 1}`)})`,
            values: ids
        }
        await typeOrm.q(query.text, query.values)
    }
}
