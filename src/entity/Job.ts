import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    BaseEntity,
    JoinColumn,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne,
    OneToOne,
    PrimaryColumn
} from 'typeorm'

import typeOrm from '../services/typeOrm'
import { User } from './User'
import { Company } from './Company'
import { Email } from './Email'

@Entity({ name: 'jobs' })
export class Job extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @ManyToOne(() => User, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'created_id' })
    user: User;

    @ManyToOne(() => Company, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'company_id' })
    company: Company;

    @OneToOne(() => Job, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'parent_id' })
    jobParent: Job;

    @Column({ nullable: true, type: 'bigint' })
    created_id: number;

    @Column({ nullable: true, type: 'bigint' })
    company_id: number;

    @Column({ nullable: true, type: 'bigint' })
    parent_id: number;

    @Column({ nullable: true })
    title: string;

    @Column({ nullable: true, type: 'text' })
    description: string;

    @Column({ nullable: true, type: 'json' })
    positions: Array<any>;

    @Column({ nullable: true, type: 'json' })
    location: object;

    @Column({ nullable: true, type: 'json' })
    job_scope: object;

    @Column({ nullable: true, type: 'json' })
    work_availability: object;

    @Column({ nullable: true, type: 'float' })
    salary: number;

    @Column({ nullable: true })
    priority: string;

    @Column({ nullable: true })
    num_of_employees: number;

    @Column({ nullable: true })
    num_of_employees_occupied: number;

    @Column({ nullable: true })
    minimum_match_lvl: number;

    @Column({ nullable: true, type: 'timestamp' })
    due_date: Date;

    @Column({ nullable: true })
    status: string;

    @Column({ nullable: true, type: 'json' })
    watched: Array<any>;

    @Column({ nullable: true, type: 'json' })
    characterize_candidate: Array<any>;

    @Column({ nullable: true, type: 'json' })
    advertise: Array<any>;

    @Column({ nullable: true, type: 'json' })
    processes: Array<any>;

    @Column({ nullable: true, type: 'json' })
    notes: Array<any>;

    @Column({ nullable: true, type: 'json' })
    approve_by_manager: object;

    @Column({ nullable: true, type: 'json' })
    send_documents: Array<any>;

    @OneToOne(() => Email, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'start_email' })
    startEmail: Email;

    @OneToOne(() => Email, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'hire_email' })
    hireEmail: Email;

    @OneToOne(() => Email, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'reject_email' })
    rejectEmail: Email;

    @Column({ nullable: true })
    start_email: number;

    @Column({ nullable: true })
    hire_email: number;

    @Column({ nullable: true })
    reject_email: number;

    @CreateDateColumn({ type: 'timestamp' })
    created_at: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    updated_at: Date;

    @Column({ type: 'timestamp', nullable: true })
    finished_at: Date;

    @Column({ type: 'timestamp', nullable: true })
    deleted_at: Date;

    static async softRemoveMany (ids: Array<number>) {
        const query = {
            text: `UPDATE candidates SET deleted_at = CURRENT_TIMESTAMP WHERE id IN(${ids.map((id, i) => `$${i + 1}`)})`,
            values: ids
        }
        await typeOrm.q(query.text, query.values)
    }
}
