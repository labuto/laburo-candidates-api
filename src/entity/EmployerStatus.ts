import {
    Entity, PrimaryGeneratedColumn, Column, BaseEntity, JoinColumn, CreateDateColumn, UpdateDateColumn,
    ManyToOne, PrimaryColumn
} from 'typeorm'

import typeOrm from '../services/typeOrm'
import { Company } from './Company'
import { User } from './User'
import {Candidate} from './Candidate'
import {Job} from './Job'
import {Employer} from './Employer'

@Entity({ name: 'employer_status' })
export class EmployerStatus extends BaseEntity {


    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @ManyToOne(() => Employer, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'employer_id' })
    employer: Employer;

    @Column({ nullable: true, type: 'bigint' })
    employer_id: number;

    @Column({ nullable: true, default: false })
    is_resign: boolean;

    @Column({ nullable: true, type: 'json' })
    notes: object;

    @CreateDateColumn({ type: 'timestamp' })
    created_at: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    updated_at: Date;

    @Column({ type: 'timestamp', nullable: true })
    deleted_at: Date;

    static async softRemoveMany (ids: Array<number>) {
        const query = {
            text: `UPDATE employers SET deleted_at = CURRENT_TIMESTAMP WHERE id IN(${ids.map((id, i) => `$${i + 1}`)})`,
            values: ids
        }
        await typeOrm.q(query.text, query.values)
    }
}
