import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    BaseEntity,
    JoinColumn,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne,
    OneToOne,
    PrimaryColumn
} from 'typeorm'

import { Job } from './Job'

@Entity({ name: 'jobsHistory' })
export class JobHistory extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @ManyToOne(() => Job, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'job_id' })
    job: Job;

    @Column({ nullable: true, type: 'bigint' })
    job_id: number;

    @Column({ nullable: true, type: 'json' })
    data: object;

    @CreateDateColumn({ type: 'timestamp' })
    created_date: Date;
}
