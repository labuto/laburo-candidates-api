import {
    Entity, PrimaryGeneratedColumn, Column, BaseEntity, JoinColumn, CreateDateColumn, UpdateDateColumn,
    ManyToOne, PrimaryColumn
} from 'typeorm'
import typeOrm from '../services/typeOrm'
import {User} from './User'

@Entity({ name: 'companies' })
export class Company extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @ManyToOne(() => User, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'created_id' })
    user: User;

    @Column({ nullable: true, type: 'bigint' })
    created_id: number;

    @Column({ nullable: true })
    title: string;

    @Column({ nullable: true, type: 'text' })
    description: string;

    @Column({ nullable: true })
    type: string;

    @Column({ nullable: true, length: 255 })
    photo: string;

    @Column({ nullable: true })
    size: string;

    @Column({ nullable: true, length: 255 })
    location: string;

    @CreateDateColumn({ type: 'timestamp' })
    created_at: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    updated_at: Date;

    @Column({ type: 'timestamp', nullable: true })
    deleted_at: Date;

    static async softRemoveMany (ids: Array<number>) {
        const query = {
            text: `UPDATE candidates SET deleted_at = CURRENT_TIMESTAMP WHERE id IN(${ids.map((id, i) => `$${i + 1}`)})`,
            values: ids
        }
        await typeOrm.q(query.text, query.values)
    }
}
