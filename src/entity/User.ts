import {
    Entity,
    PrimaryGeneratedColumn,
    BaseEntity,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    Index,
    PrimaryColumn
} from 'typeorm'
import typeOrm from '../services/typeOrm'

@Index(['username', 'email'], { unique: true })
@Entity({ name: 'users' })
export class User extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @Column({ length: 255 })
    email: string;

    @Column({ nullable: true })
    username: string;

    @Column({ nullable: true })
    hash: string;

    @Column({ nullable: true })
    role: string;

    @Column({ nullable: true })
    permission: number;

    @Column({ type: 'timestamp', nullable: true })
    blocked_at: Date;

    @CreateDateColumn({ type: 'timestamp' })
    created_at: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    updated_at: Date;

    @Column({ type: 'timestamp', nullable: true })
    deleted_at: Date;

    static async softRemoveMany (ids: Array<number>) {
        const query = {
            text: `UPDATE users SET deleted_at = CURRENT_TIMESTAMP WHERE id IN(${ids.map((id, i) => `$${i + 1}`)})`,
            values: ids
        }
        await typeOrm.q(query.text, query.values)
    }
}
