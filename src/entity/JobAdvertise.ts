import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    BaseEntity,
    JoinColumn,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne,
} from 'typeorm'

import typeOrm from '../services/typeOrm'
import { Job } from './Job'
import { Company } from './Company'

@Entity({ name: 'job_advertise' })
export class JobAdvertise extends BaseEntity {

    @PrimaryGeneratedColumn({  type: 'bigint' })
    id: number;

    @ManyToOne(() => Job, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'job_id' })
    job: Job;

    @ManyToOne(() => Company, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'company_id' })
    company: Company;

    @Column({ nullable: true, type: 'bigint' })
    job_id: number;

    @Column({ nullable: true, type: 'bigint' })
    company_id: number;

    @Column({ nullable: true, type: 'json' })
    title: object

    @Column({ nullable: true, type: 'json' })
    job_description: object

    @Column({ nullable: true, type: 'json' })
    positions: object

    @Column({ nullable: true, type: 'json' })
    experience: object

    @Column({ nullable: true, type: 'json' })
    educations: object

    @Column({ nullable: true, type: 'json' })
    military: object

    @Column({ nullable: true, type: 'json' })
    languages: object

    @Column({ nullable: true, type: 'json' })
    job_scope: object

    @Column({ nullable: true, type: 'json' })
    work_availability: object

    @Column({ nullable: true, type: 'json' })
    location: object

    @Column({ nullable: true, type: 'json' })
    salary: object

    @Column({ nullable: true, type: 'json' })
    present_job_process: object

    @Column({ nullable: true, type: 'json' })
    present_company_details: object

    @Column({ nullable: true, type: 'json' })
    image: object

    @Column({ nullable: true, type: 'json' })
    customize: object

    @CreateDateColumn({ type: 'timestamp' })
    created_at: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    updated_at: Date;

    @Column({ type: 'timestamp', nullable: true })
    deleted_at: Date;

    static async softRemoveMany (ids: Array<number>) {
        const query = {
            text: `UPDATE job_advertise SET deleted_at = CURRENT_TIMESTAMP WHERE id IN(${ids.map((id, i) => `$${i + 1}`)})`,
            values: ids
        }
        await typeOrm.q(query.text, query.values)
    }
}
