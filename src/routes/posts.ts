import { Application } from 'express'
const { auth, asyncWrap } = require('../middleware')
import PostController from '../controllers/post'

module.exports = (app: Application) => {
    app.post('/posts/getPosts', auth, asyncWrap(PostController.getPosts))

    app.post('/posts/addPost', auth, asyncWrap(PostController.addPost))

    app.post('/posts/removePost', auth, asyncWrap(PostController.removePost))

    app.post('/posts/addComment', auth, asyncWrap(PostController.addComment))

    app.post('/posts/removeComment', auth, asyncWrap(PostController.removeComment))

    app.post('/posts/addSubComment', auth, asyncWrap(PostController.addSubComment))

    app.post('/posts/removeSubComment', auth, asyncWrap(PostController.removeSubComment))

    app.post('/posts/addLike', auth, asyncWrap(PostController.addLike))

    app.post('/posts/addNotLike', auth, asyncWrap(PostController.addNotLike))
}
