import { Application } from 'express'
const { auth, asyncWrap } = require('../middleware')
import JobController from '../controllers/job'

module.exports = (app: Application) => {
    app.post('/jobs/get', auth, asyncWrap(JobController.get))

    app.post('/jobs/getById', auth, asyncWrap(JobController.getById))

    app.post('/jobs/getHistory', auth, asyncWrap(JobController.getHistory))

    app.post('/jobs/addCandidateWatch', auth, asyncWrap(JobController.addCandidateWatch))

    app.post('/jobs/addCandidateRemove', auth, asyncWrap(JobController.addCandidateRemove))

    app.post('/jobs/removeCandidateRemove', auth, asyncWrap(JobController.removeCandidateRemove))

    app.post('/jobs/addCandidateSent', auth, asyncWrap(JobController.addCandidateSent))
}
