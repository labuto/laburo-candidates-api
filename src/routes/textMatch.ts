import { Application } from 'express'
const { auth, asyncWrap } = require('../middleware')
import TextMatchController from '../controllers/textMatch'

module.exports = (app: Application) => {
    app.post('/textMatch/get', auth, asyncWrap(TextMatchController.get))

    app.post('/textMatch/add', auth, asyncWrap(TextMatchController.add))

    app.post('/textMatch/update', auth, asyncWrap(TextMatchController.update))

    app.post('/textMatch/remove', auth, asyncWrap(TextMatchController.remove))
}