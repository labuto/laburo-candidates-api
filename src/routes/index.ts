import express, { Router } from 'express'
const apiSubRouter = express.Router()

const edges: Array<string> = [
    'candidates',
    'jobs',
    'requirements',
    'similarMatch',
    'textMatch',
    'contact',
    'posts'
]

export default (apiRouter: Router) => {
    for (const edge of edges) {
        const api = require(`./${edge}`)
        api(apiSubRouter)
        apiRouter.use(``, apiSubRouter)
    }
}
