import { Application } from 'express'
const { auth, asyncWrap } = require('../middleware')
import ContactController from '../controllers/contact'

module.exports = (app: Application) => {
    app.post('/contact/addMessage', auth, asyncWrap(ContactController.addMessage))
}
