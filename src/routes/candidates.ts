import { Application } from 'express'
const { auth, asyncWrap } = require('../middleware')
import CandidatesController from '../controllers/candidates'

module.exports = (app: Application) => {

    // GET SINGLE
    app.post('/candidates/getBasicById/', auth, asyncWrap(CandidatesController.getBasicById))

    app.post('/candidates/getFullById', auth, asyncWrap(CandidatesController.getFullById))

    app.post('/candidates/getJobSearchById', auth, asyncWrap(CandidatesController.getJobSearchById))

    app.post('/candidates/getByEmail', auth, asyncWrap(CandidatesController.getByEmail))



    // ADD / UPDATE
    app.post('/candidates/add/', auth, asyncWrap(CandidatesController.add))

    app.post('/candidates/updateAchievement/', auth, asyncWrap(CandidatesController.updateAchievement))

    app.post('/candidates/removeAchievement/', auth, asyncWrap(CandidatesController.removeAchievement))

    app.post('/candidates/updateJobSearch/', auth, asyncWrap(CandidatesController.updateJobSearch))

    app.post('/candidates/removeJobSearch/', auth, asyncWrap(CandidatesController.removeJobSearch))

    app.post('/candidates/updateSingleData/', auth, asyncWrap(CandidatesController.updateSingleData))

    app.post('/candidates/updateGeneral/', auth, asyncWrap(CandidatesController.updateGeneral))

    app.post('/candidates/changePassword/', auth, asyncWrap(CandidatesController.changePassword))

    app.post('/candidates/updateShare/', auth, asyncWrap(CandidatesController.updateShare))

    app.post('/candidates/updateResumeName/', auth, asyncWrap(CandidatesController.updateResumeName))



    // CHECK
    app.post('/candidates/checkCandidateExist/', auth, asyncWrap(CandidatesController.checkCandidateExist))



    // VALIDATION KEYS
    app.post('/candidates/addCandidateKey/', auth, asyncWrap(CandidatesController.addCandidateKey))

    app.post('/candidates/removeCandidateKey/', auth, asyncWrap(CandidatesController.removeCandidateKey))

    app.post('/candidates/validCandidateKey/', auth, asyncWrap(CandidatesController.validCandidateKey))
}