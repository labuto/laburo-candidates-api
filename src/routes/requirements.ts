import { Application } from 'express'
const { auth, asyncWrap } = require('../middleware')
import RequirementsController from '../controllers/requirements'

module.exports = (app: Application) => {

    app.post('/requirements/getAll/', auth, asyncWrap(RequirementsController.getAll))
}
