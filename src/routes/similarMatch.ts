import { Application } from 'express'
const { auth, asyncWrap } = require('../middleware')
import SimilarMatchController from '../controllers/similarMatch'

module.exports = (app: Application) => {
    app.post('/similarMatch/get', auth, asyncWrap(SimilarMatchController.get))

    app.post('/similarMatch/add', auth, asyncWrap(SimilarMatchController.add))

    app.post('/similarMatch/update', auth, asyncWrap(SimilarMatchController.update))

    app.post('/similarMatch/remove', auth, asyncWrap(SimilarMatchController.remove))
}