const { Listener } = require('../services')

export default {
  init: (server: any) => {
    const io = require('socket.io')(server, { 'forceNew': true })

    io.on('connection', (socket: any) => {
      console.log('socket connected', `client id ${socket.client.id}`)
      let listener = new Listener(socket)
      socket.on('disconnect', () => {
        listener.destroy()
        listener = null
        console.log(`socket ${socket.client.id} disconnected`)
        socket.disconnect()
      })
    })
  }
}
