import { Request, Response } from 'express'
import { QueryInterface } from '../interface/query'
import {Post} from '../entity/Post/Post'
import {Comment} from '../entity/Post/Comment'
import {SubComment} from '../entity/Post/SubComment'
import {Candidate, CandidateGeneral} from '../entity/Candidate'
import {PostLike} from '../entity/Post/PostLike'
import {CommentLike} from '../entity/Post/CommentLike'
import { SubCommentLike } from '../entity/Post/SubCommentLike';
import { PostNotLike } from '../entity/Post/PostNotLike';
import { CommentNotLike } from '../entity/Post/CommentNotLike';
import { SubCommentNotLike } from '../entity/Post/SubCommentNotLike';
const { errorHandler, typeOrm, validation } = require('../services')

class PostController {
    getPosts = async (req: Request, res: Response) => {
        try {
            const { candidateId } = req.body

            res.send([])
        } catch (err) {
            errorHandler.basicHandler('posts/getPosts', err, res)
        }
    }

    addPost = async (req: Request, res: Response) => {
        try {
            const body: object = {
                candidate_id: req.body.candidate_id,
                is_laburo: req.body.is_laburo,
                content: req.body.content,
                img: req.body.img,
                filling: req.body.filling,
                privacy: req.body.privacy
            }

            const post: Post = await Post.create(body).save()

            const author = await this.loadAuthorData(post.candidate_id, post.privacy)
            Object.assign(post, {
                author,
                is_create_comment_open: false,
                is_comments_open: false,
                likes: [],
                not_likes: [],
                comments: []
            })

            res.send(post)
        } catch (err) {
            errorHandler.basicHandler('posts/addPost', err, res)
        }
    }

    removePost = async (req: Request, res: Response) => {
        try {
            const { id } = req.body

            await Post.softRemoveMany([id])

            res.end()
        } catch (err) {
            errorHandler.basicHandler('posts/removePost', err, res)
        }
    }

    addComment = async (req: Request, res: Response) => {
        try {
            const body: object = {
                post_id: req.body.post_id,
                candidate_id: req.body.candidate_id,
                is_laburo: req.body.is_laburo,
                content: req.body.content,
                img: req.body.img,
                filling: req.body.filling,
                privacy: req.body.privacy
            }

            const comment: Comment = await Comment.create(body).save()

            const author = await this.loadAuthorData(comment.candidate_id, comment.privacy)
            const responseComment: any = {
                data: comment,
                author,
                is_comments_open: false,
                likes: [],
                not_likes: [],
                sub_comments: []
            }

            res.send(responseComment)
        } catch (err) {
            errorHandler.basicHandler('posts/addComment', err, res)
        }
    }

    removeComment = async (req: Request, res: Response) => {
        try {
            const { id } = req.body

            await Comment.softRemoveMany([id])

            res.end()
        } catch (err) {
            errorHandler.basicHandler('posts/removeComment', err, res)
        }
    }

    addSubComment = async (req: Request, res: Response) => {
        try {
            const body: object = {
                comment_id: req.body.comment_id,
                candidate_id: req.body.candidate_id,
                is_laburo: req.body.is_laburo,
                content: req.body.content,
                filling: req.body.filling,
                privacy: req.body.privacy
            }

            const subComment: SubComment = await SubComment.create(body).save()

            const author = await this.loadAuthorData(subComment.candidate_id, subComment.privacy)
            const responseSubComment: any = {
                data: subComment,
                author,
                likes: [],
                not_likes: []
            }

            res.send(responseSubComment)
        } catch (err) {
            errorHandler.basicHandler('posts/addSubComment', err, res)
        }
    }

    removeSubComment = async (req: Request, res: Response) => {
        try {
            const { id } = req.body

            await SubComment.softRemoveMany([id])

            res.end()
        } catch (err) {
            errorHandler.basicHandler('posts/removeSubComment', err, res)
        }
    }

    addLike = async (req: Request, res: Response) => {
        try {
            const { id, type, candidateId } = req.body

            switch (type) {
                case 'post':
                    await PostLike.create({ candidate_id: candidateId, post_id: id }).save()
                    break
                case 'comment':
                    await CommentLike.create({ candidate_id: candidateId, comment_id: id }).save()
                    break
                case 'subComment':
                    await SubCommentLike.create({ candidate_id: candidateId, sub_comment_id: id }).save()
                    break
            }

            const author = await this.loadAuthorData(candidateId, '')

            res.send(author)
        } catch (err) {
            errorHandler.basicHandler('posts/addLike', err, res)
        }
    }

    addNotLike = async (req: Request, res: Response) => {
        try {
            const { id, type, candidateId } = req.body

            switch (type) {
                case 'post':
                    await PostNotLike.create({ candidate_id: candidateId, post_id: id }).save()
                    break
                case 'comment':
                    await CommentNotLike.create({ candidate_id: candidateId, comment_id: id }).save()
                    break
                case 'subComment':
                    await SubCommentNotLike.create({ candidate_id: candidateId, sub_comment_id: id }).save()
                    break
            }

            const author = await this.loadAuthorData(candidateId, '')

            res.send(author)
        } catch (err) {
            errorHandler.basicHandler('posts/addNotLike', err, res)
        }
    }

    async loadAuthorData (candidateId: number, privacy: string) {
        let author = {}
        const candidate: Candidate | undefined = await Candidate
            .createQueryBuilder('c')
            .innerJoinAndMapOne('c.general', CandidateGeneral, 'cg', 'c.id = cg.candidate_id')
            .where('c.id = :id', { id: candidateId })
            .getOne()

        if (candidate) {
            Object.assign(author, {
                id: candidate.id,
                name: privacy === 'anonymous' ? 'XXX' : candidate.full_name,
                photo: privacy === 'anonymous' ? '' : candidate.general.photo,
                type: 'candidate'
            })
        }

        return author
    }
}

export default new PostController()
