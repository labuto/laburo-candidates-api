const fs = require('fs')
const general = JSON.parse(fs.readFileSync(__dirname + '/../../public/data/general.json', 'utf8'))
const languages = JSON.parse(fs.readFileSync(__dirname + '/../../public/data/languages.json', 'utf8'))
const positions = JSON.parse(fs.readFileSync(__dirname + '/../../public/data/positions', 'utf8'))
const education = JSON.parse(fs.readFileSync(__dirname + '/../../public/data/education.json', 'utf8'))
const military = JSON.parse(fs.readFileSync(__dirname + '/../../public/data/military.json', 'utf8'))
const countries = JSON.parse(fs.readFileSync(__dirname + '/../../public/data/countries.json', 'utf8'))

import { Request, Response } from 'express'
const { errorHandler } = require('../services')

class RequirementController {

    getAll = async (req: Request, res: Response) => {
        try {
            // get language
            const lang = req.body.lang || 'en'

            // get all fields
            const fields: any = {
                general: this.getGeneralData(lang),
                positions: this.getPositionsData(lang),
                education: this.getEducationData(lang),
                military: this.getMilitaryData(lang),
                languagesData: this.getLanguagesData(lang),
                countriesCities: this.getCountriesCitiesData(lang)
            }

            // send field
            res.send(fields)
        } catch (err) { errorHandler.basicHandler(err) }
    }

    getGeneralObject = (key: string) => {
        return general[key].values
    }

    getPositionValue = (position: any) => {
        if (!position) { return { en: '', he: '' } }
        return positions[position.field].positions[position.position]
    }

    /* GENERAL DATA */

    getGeneral = (req: Request, res: Response) => {
        try {
            // get language
            const lang = req.body.lang || 'en'

            // get general data
            const generalData = this.getGeneralData(lang)

            // send general data
            res.send(generalData)
        } catch (err) { errorHandler.basicHandler(err) }
    }

    getGeneralData (lang: string = 'en') {
        const generalData: any = {}
        Object.entries(general).forEach((g: any) => {
            generalData[g[0]] = {
                label: g[1][lang],
                values: Object.entries(g[1].values).map((v: any) => ({
                    value: v[0],
                    label: v[1][lang],
                    icon: v[1].icon
                }))
            }
        })

        return generalData
    }

    getFullGeneral = (req: Request, res: Response) => {
        try {
            // get language
            const lang: string = req.body.lang || 'en'

            // get general data
            const generalData = this.getFullGeneralData(lang)

            // send general data
            res.send(generalData)
        } catch (err) { errorHandler.basicHandler(err) }
    }

    getFullGeneralData (lang: string = 'en') {
        const generalData: any = Object.entries(general)
            .filter((f: any) => f[1][lang])
            .sort((a: any, b: any) => a[1][lang].toLowerCase().trim().localeCompare(b[1][lang].toLowerCase().trim()))

        return generalData
    }

    addGeneralField = async (req: Request, res: Response) => {
        try {
            const newField: any = req.body.newField

            // check if field already exist in list
            if (general[newField.en]) {
                console.error('Field ' + general[newField.en] + ' already exist in general')
                throw new Error
            }

            // add new field with empty position
            general[newField.en] = {
                en: newField.en,
                he: newField.he,
                values: {}
            }

            // fill all positions for new field
            newField.values.forEach((p: any) => {
                general[newField.en].values[p.en] = {
                    en: p.en,
                    he: p.he
                }
            })

            // write back to file
            fs.writeFileSync(__dirname + '/../../public/data/general.json', JSON.stringify(general))

            // get position data
            const generalData = this.getFullGeneralData()

            // send position data
            res.send(generalData)
        } catch (err) { errorHandler.basicHandler(err) }
    }

    removeGeneralField = async (req: Request, res: Response) => {
        try {
            const field: any = req.body.field

            // check if field exist in list
            if (!general[field]) {
                console.error('Field ' + general[field] + ' not exist in general')
                throw new Error
            }

            // remove field from general list
            delete general[field]

            // write back to file
            fs.writeFileSync(__dirname + '/../../public/data/general.json', JSON.stringify(general))

            // get position data
            const generalData = this.getFullGeneralData()

            // send position data
            res.send(generalData)
        } catch (err) { errorHandler.basicHandler(err) }
    }

    updateGeneralField = (req: Request, res: Response) => {
        try {
            const { field, newValue, lang } = req.body

            // check if field exist in list
            if (!general[field]) {
                console.error('Field ' + general[field] + ' not exist in general')
                throw new Error
            }

            // update field in general list
            general[field][lang] = newValue

            // write back to file
            fs.writeFileSync(__dirname + '/../../public/data/general.json', JSON.stringify(general))

            res.end()
        } catch (err) { errorHandler.basicHandler(err) }
    }

    addGeneralValues = async (req: Request, res: Response) => {
        try {
            const { field, newValues } = req.body

            // check if field already exist in list
            if (!general[field]) {
                console.error('Field ' + general[field] + ' not exist in general')
                throw new Error
            }

            // fill all general for new field
            newValues.forEach((p: any) => {
                general[field].values[p.en] = {
                    en: p.en,
                    he: p.he
                }
            })

            // write back to file
            fs.writeFileSync(__dirname + '/../../public/data/general.json', JSON.stringify(general))

            // get general data
            const generalData = this.getFullGeneralData()

            // send general data
            res.send(generalData)
        } catch (err) { errorHandler.basicHandler(err) }
    }

    removeGeneralValue = async (req: Request, res: Response) => {
        try {
            const { field, value }  = req.body

            // check if field exist in list
            if (!general[field]) {
                console.error('Field ' + general[field] + ' not exist in general')
                throw new Error
            }


            // check if general exist in list
            if (!general[field].values[value]) {
                console.error('Value ' + general[field].values[value] + ' not exist in general')
                throw new Error
            }

            // remove value from general list
            delete general[field].values[value]

            // write back to file
            fs.writeFileSync(__dirname + '/../../public/data/general.json', JSON.stringify(general))

            // get general data
            const generalsData = this.getFullGeneralData()

            // send general data
            res.send(generalsData)
        } catch (err) { errorHandler.basicHandler(err) }
    }

    updateGeneralValue = (req: Request, res: Response) => {
        try {
            const { field, value, newValue, lang } = req.body

            // check if field exist in list
            if (!general[field]) {
                console.error('Field ' + general[field] + ' not exist in general')
                throw new Error
            }

            // check if general exist in list
            if (!general[field].values[value]) {
                console.error('Value ' + general[field].values[value] + ' not exist in general')
                throw new Error
            }

            // update value in general list
            general[field].values[value][lang] = newValue

            // write back to file
            fs.writeFileSync(__dirname + '/../../public/data/general.json', JSON.stringify(general))

            res.end()
        } catch (err) { errorHandler.basicHandler(err) }
    }


    /* LANGUAGE DATA */

    getLanguages = (req: Request, res: Response) => {
        try {
            // get language
            const lang: string = req.body.lang || 'en'

            // get position data
            const languagesData = this.getLanguagesData(lang)

            // send language data
            res.send(languagesData)
        } catch (err) { errorHandler.basicHandler(err) }
    }

    getLanguagesData (lang = 'en') {
        const languagesData = Object.entries(languages)
            .filter((f: any) => f[1][lang])
            .sort((a: any, b: any) => a[1][lang].toLowerCase().trim().localeCompare(b[1][lang].toLowerCase().trim()))

        return languagesData
    }


    /* POSITION DATA */

    getPositions = (req: Request, res: Response) => {
        try {
            // get language
            const lang = req.body.lang || 'en'

            // get position data
            const positionsData = this.getPositionsData(lang)

            // send position data
            res.send(positionsData)
        } catch (err) { errorHandler.basicHandler(err) }
    }

    getPositionsData (lang = 'en') {
        const positionsData = Object.entries(positions)
            .filter((f: any) => f[1][lang] && f[1].positions && Object.keys(f[1].positions).length)
            .sort((a: any, b: any) => a[1][lang].toLowerCase().trim().localeCompare(b[1][lang].toLowerCase().trim()))

        return positionsData
    }

    addPositionField = async (req: Request, res: Response) => {
        try {
            const newField = req.body.newField

            // check if field already exist in list
            if (positions[newField.en]) {
                console.error('Field ' + positions[newField.en] + ' already exist in positions')
                throw new Error
            }

            // add new field with empty position
            positions[newField.en] = {
                en: newField.en,
                he: newField.he,
                positions: {}
            }

            // fill all positions for new field
            newField.positions.forEach((p: any) => {
                positions[newField.en].positions[p.en] = {
                    en: p.en,
                    he: p.he
                }
            })

            // write back to file
            fs.writeFileSync(__dirname + '/../../public/data/positions', JSON.stringify(positions))

            // get position data
            const positionsData = this.getPositionsData()

            // send position data
            res.send(positionsData)
        } catch (err) { errorHandler.basicHandler(err) }
    }

    removePositionField = async (req: Request, res: Response) => {
        try {
            const field = req.body.field

            // check if field exist in list
            if (!positions[field]) {
                console.error('Field ' + positions[field] + ' not exist in positions')
                throw new Error
            }

            // remove field from positions list
            delete positions[field]

            // write back to file
            fs.writeFileSync(__dirname + '/../../public/data/positions', JSON.stringify(positions))

            // get position data
            const positionsData = this.getPositionsData()

            // send position data
            res.send(positionsData)
        } catch (err) { errorHandler.basicHandler(err) }
    }

    updatePositionField = (req: Request, res: Response) => {
        try {
            const { field, newValue, lang } = req.body

            // check if field exist in list
            if (!positions[field]) {
                console.error('Field ' + positions[field] + ' not exist in positions')
                throw new Error
            }

            // update field in positions list
            positions[field][lang] = newValue

            // write back to file
            fs.writeFileSync(__dirname + '/../../public/data/positions', JSON.stringify(positions))

            res.end()
        } catch (err) { errorHandler.basicHandler(err) }
    }

    addPositionPositions = async (req: Request, res: Response) => {
        try {
            const { field, newPositions } = req.body

            // check if field already exist in list
            if (!positions[field]) {
                console.error('Field ' + positions[field] + ' not exist in positions')
                throw new Error
            }

            // fill all positions for new field
            newPositions.forEach((p: any) => {
                positions[field].positions[p.en] = {
                    en: p.en,
                    he: p.he
                }
            })

            // write back to file
            fs.writeFileSync(__dirname + '/../../public/data/positions', JSON.stringify(positions))

            // get position data
            const positionsData = this.getPositionsData()

            // send position data
            res.send(positionsData)
        } catch (err) { errorHandler.basicHandler(err) }
    }

    removePositionPosition = async (req: Request, res: Response) => {
        try {
            const { field, position }  = req.body

            // check if field exist in list
            if (!positions[field]) {
                console.error('Field ' + positions[field] + ' not exist in positions')
                throw new Error
            }


            // check if position exist in list
            if (!positions[field].positions[position]) {
                console.error('Position ' + positions[field].positions[position] + ' not exist in positions')
                throw new Error
            }

            // remove field from positions list
            delete positions[field].positions[position]

            // write back to file
            fs.writeFileSync(__dirname + '/../../public/data/positions', JSON.stringify(positions))

            // get position data
            const positionsData = this.getPositionsData()

            // send position data
            res.send(positionsData)
        } catch (err) { errorHandler.basicHandler(err) }
    }

    updatePositionPosition = (req: Request, res: Response) => {
        try {
            const { field, position, newValue, lang } = req.body

            // check if field exist in list
            if (!positions[field]) {
                console.error('Field ' + positions[field] + ' not exist in positions')
                throw new Error
            }

            // check if position exist in list
            if (!positions[field].positions[position]) {
                console.error('Position ' + positions[field].positions[position] + ' not exist in positions')
                throw new Error
            }

            // update field in positions list
            positions[field].positions[position][lang] = newValue

            // write back to file
            fs.writeFileSync(__dirname + '/../../public/data/positions', JSON.stringify(positions))

            res.end()
        } catch (err) { errorHandler.basicHandler(err) }
    }


    /* EDUCATION DATA */

    getEducation = (req: Request, res: Response) => {
        try {
            // get language
            const lang: string = req.body.lang || 'en'

            // get education data
            const educationData = this.getEducationData(lang)

            // send education data
            res.send(educationData)
        } catch (err) { errorHandler.basicHandler(err) }
    }

    getEducationData (lang: string = 'en') {
        const localData = JSON.parse(JSON.stringify(education))
        const educationData = Object.entries(localData)
            .filter((f: any) => f[1][lang])
            .sort((a: any, b: any) => a[1][lang].toLowerCase().trim().localeCompare(b[1][lang].toLowerCase().trim()))

        educationData.forEach((edu: any) => {
            edu[1].values = Object.entries(edu[1].values)
                .filter((f: any) => f[1][lang])
                .sort((a: any, b: any) => a[1][lang].toLowerCase().trim().localeCompare(b[1][lang].toLowerCase().trim()))
        })

        return educationData
    }

    addEducationField = async (req: Request, res: Response) => {
        try {
            const { type, newField } = req.body

            // check if field already exist in list
            if (education[type] && education[type].values[newField.en]) {
                console.error('Field ' + education[type].values[newField.en] + ' already exist in education')
                throw new Error
            }

            // add new field with empty education
            education[type].values[newField.en] = {
                en: newField.en,
                he: newField.he,
                diplomas: {}
            }

            // fill all diplomas for new field
            newField.diplomas.forEach((d: any) => {
                education[type].values[newField.en].diplomas[d.en] = {
                    en: d.en,
                    he: d.he,
                    courses: {},
                    institutes: {}
                }

                // fill all institutes for new field
                d.institutes.forEach((i: any) => {
                    education[type].values[newField.en].diplomas[d.en].institutes[i.en] = {
                        en: i.en,
                        he: i.he
                    }
                })
            })

            // write back to file
            fs.writeFileSync(__dirname + '/../../public/data/education.json', JSON.stringify(education))

            // get education data
            const educationData = this.getEducationData()

            // send education data
            res.send(educationData)
        } catch (err) { errorHandler.basicHandler(err) }
    }

    removeEducationField = async (req: Request, res: Response) => {
        try {
            const { type, field } = req.body

            // check if field exist in list
            if (!education[type] || !education[type].values[field]) {
                console.error('Field ' + education[type].values[field] + ' not exist in education')
                throw new Error
            }

            // remove field from education list
            delete education[type].values[field]

            // write back to file
            fs.writeFileSync(__dirname + '/../../public/data/education.json', JSON.stringify(education))

            // get education data
            const educationData = this.getEducationData()

            // send education data
            res.send(educationData)
        } catch (err) { errorHandler.basicHandler(err) }
    }

    updateEducationField = (req: Request, res: Response) => {
        try {
            const { type, field, newValue, lang } = req.body

            // check if field exist in list
            if (!education[type] || !education[type].values[field]) {
                console.error('Field ' + education[type].values[field] + ' not exist in education')
                throw new Error
            }

            // update field in education list
            education[type].values[field][lang] = newValue

            // write back to file
            fs.writeFileSync(__dirname + '/../../public/data/education.json', JSON.stringify(education))

            res.end()
        } catch (err) { errorHandler.basicHandler(err) }
    }

    addEducationDiplomas = async (req: Request, res: Response) => {
        try {
            const { type, field, diplomas } = req.body

            // check if field already exist in list
            if (!education[type] || !education[type].values[field]) {
                console.error('Field ' + education[type].values[field] + ' not exist in education')
                throw new Error
            }

            // fill all diplomas for field
            diplomas.forEach((d: any) => {
                education[type].values[field].diplomas[d.en] = {
                    en: d.en,
                    he: d.he,
                    courses: {},
                    institutes: {}
                }

                // fill all institutes for new field
                d.institutes.forEach((i: any) => {
                    education[type].values[field].diplomas[d.en].institutes[i.en] = {
                        en: i.en,
                        he: i.he
                    }
                })
            })

            // write back to file
            fs.writeFileSync(__dirname + '/../../public/data/education.json', JSON.stringify(education))

            // get education data
            const educationData = this.getEducationData()

            // send education data
            res.send(educationData)
        } catch (err) { errorHandler.basicHandler(err) }
    }

    removeEducationDiploma = async (req: Request, res: Response) => {
        try {
            const { type, field, diploma }  = req.body

            // check if field exist in list
            if (!education[type] || !education[type].values[field]) {
                console.error('Field ' + education[type].values[field] + ' not exist in education')
                throw new Error
            }


            // check if education exist in list
            if (!education[type].values[field].diplomas[diploma]) {
                console.error('Diploma ' + education[type].values[field].diplomas[diploma] + ' not exist in education')
                throw new Error
            }

            // remove field from education list
            delete education[type].values[field].diplomas[diploma]

            // write back to file
            fs.writeFileSync(__dirname + '/../../public/data/education.json', JSON.stringify(education))

            // get education data
            const educationData = this.getEducationData()

            // send education data
            res.send(educationData)
        } catch (err) { errorHandler.basicHandler(err) }
    }

    updateEducationDiploma = (req: Request, res: Response) => {
        try {
            const { type, field, diploma, newValue, lang } = req.body

            // check if field exist in list
            if (!education[type] || !education[type].values[field]) {
                console.error('Field ' + education[type].values[field] + ' not exist in education')
                throw new Error
            }

            // check if education exist in list
            if (!education[type].values[field].diplomas[diploma]) {
                console.error('Diploma ' + education[type].values[field].diplomas[diploma] + ' not exist in education')
                throw new Error
            }

            // update field in education list
            education[type].values[field].diplomas[diploma][lang] = newValue

            // write back to file
            fs.writeFileSync(__dirname + '/../../public/data/education.json', JSON.stringify(education))

            res.end()
        } catch (err) { errorHandler.basicHandler(err) }
    }

    addEducationInstitutes = async (req: Request, res: Response) => {
        try {
            const { type, field, diploma, institutes } = req.body

            // check if field not exist in list
            if (!education[type] || !education[type].values[field]) {
                console.error('Field ' + education[type].values[field] + ' not exist in education')
                throw new Error
            }

            // check if diploma already exist in list
            if (!education[type].values[field].diplomas[diploma]) {
                console.error('Diploma ' + education[type].values[field].diplomas[diploma] + ' not exist in education')
                throw new Error
            }

            // fill all institutes for new field
            institutes.forEach((i: any) => {
                education[type].values[field].diplomas[diploma].institutes[i.en] = {
                    en: i.en,
                    he: i.he
                }
            })

            // write back to file
            fs.writeFileSync(__dirname + '/../../public/data/education.json', JSON.stringify(education))

            // get education data
            const educationData = this.getEducationData()

            // send education data
            res.send(educationData)
        } catch (err) { errorHandler.basicHandler(err) }
    }

    removeEducationInstitute = async (req: Request, res: Response) => {
        try {
            const { type, field, diploma, institute }  = req.body

            // check if field exist in list
            if (!education[type] || !education[type].values[field]) {
                console.error('Field ' + education[type].values[field] + ' not exist in education')
                throw new Error
            }


            // check if education exist in list
            if (!education[type].values[field].diplomas[diploma]) {
                console.error('Diploma ' + education[type].values[field].diplomas[diploma] + ' not exist in education')
                throw new Error
            }

            // check if institute exist in list
            if (!education[type].values[field].diplomas[diploma].institutes[institute]) {
                console.error('Institute ' + education[type].values[field].diplomas[diploma].institutes[institute] + ' not exist in education')
                throw new Error
            }

            // remove institute from education list
            delete education[type].values[field].diplomas[diploma].institutes[institute]

            // write back to file
            fs.writeFileSync(__dirname + '/../../public/data/education.json', JSON.stringify(education))

            // get education data
            const educationData = this.getEducationData()

            // send education data
            res.send(educationData)
        } catch (err) { errorHandler.basicHandler(err) }
    }

    updateEducationInstitute = (req: Request, res: Response) => {
        try {
            const { type, field, diploma, institute, newValue, lang } = req.body

            // check if field exist in list
            if (!education[type] || !education[type].values[field]) {
                console.error('Field ' + education[type].values[field] + ' not exist in education')
                throw new Error
            }

            // check if education exist in list
            if (!education[type].values[field].diplomas[diploma]) {
                console.error('Diploma ' + education[type].values[field].diplomas[diploma] + ' not exist in education')
                throw new Error
            }

            // check if institute exist in list
            if (!education[type].values[field].diplomas[diploma].institutes[institute]) {
                console.error('Institute ' + education[type].values[field].diplomas[diploma].institutes[institute] + ' not exist in education')
                throw new Error
            }

            // update field in education list
            education[type].values[field].diplomas[diploma].institutes[institute][lang] = newValue

            // write back to file
            fs.writeFileSync(__dirname + '/../../public/data/education.json', JSON.stringify(education))

            res.end()
        } catch (err) { errorHandler.basicHandler(err) }
    }


    /* MILITARY DATA */

    getMilitary = (req: Request, res: Response) => {
        try {
            // get language
            const lang = req.body.lang || 'en'

            const militaryData = this.getMilitaryData(lang)

            res.send(militaryData)
        } catch (err) { errorHandler.basicHandler(err) }
    }

    getMilitaryData (lang = 'en') {
        const militaryData = Object.entries(military)
            .filter((f: any) => f[1][lang])
            .sort((a: any, b: any) => a[1][lang].toLowerCase().trim().localeCompare(b[1][lang].toLowerCase().trim()))

        return militaryData
    }

    addMilitaryField = async (req: Request, res: Response) => {
        try {
            const newField = req.body.newField

            // check if field already exist in list
            if (military[newField.en]) {
                console.error('Field ' + military[newField.en] + ' already exist in military')
                throw new Error
            }

            // add new field with empty military
            military[newField.en] = {
                en: newField.en,
                he: newField.he,
                professions: {},
                departments: {}
            }

            // fill all professions for new field
            newField.professions.forEach((d: any) => {
                military[newField.en].professions[d.en] = {
                    en: d.en,
                    he: d.he
                }
            })

            // fill all departments for new field
            newField.departments.forEach((d: any) => {
                military[newField.en].departments[d.en] = {
                    en: d.en,
                    he: d.he
                }
            })

            // write back to file
            fs.writeFileSync(__dirname + '/../../public/data/military.json', JSON.stringify(military))

            // get military data
            const militaryData = this.getMilitaryData()

            // send military data
            res.send(militaryData)
        } catch (err) { errorHandler.basicHandler(err) }
    }

    removeMilitaryField = async (req: Request, res: Response) => {
        try {
            const field = req.body.field

            // check if field exist in list
            if (!military[field]) {
                console.error('Field ' + military[field] + ' not exist in military')
                throw new Error
            }

            // remove field from military list
            delete military[field]

            // write back to file
            fs.writeFileSync(__dirname + '/../../public/data/military.json', JSON.stringify(military))

            // get military data
            const militaryData = this.getMilitaryData()

            // send military data
            res.send(militaryData)
        } catch (err) { errorHandler.basicHandler(err) }
    }

    updateMilitaryField = (req: Request, res: Response) => {
        try {
            const { field, newValue, lang } = req.body

            // check if field exist in list
            if (!military[field]) {
                console.error('Field ' + military[field] + ' not exist in military')
                throw new Error
            }

            // update field in military list
            military[field][lang] = newValue

            // write back to file
            fs.writeFileSync(__dirname + '/../../public/data/military.json', JSON.stringify(military))

            res.end()
        } catch (err) { errorHandler.basicHandler(err) }
    }

    addMilitaryProfessions = async (req: Request, res: Response) => {
        try {
            const { field, professions } = req.body

            // check if field already exist in list
            if (!military[field]) {
                console.error('Field ' + military[field] + ' not exist in military')
                throw new Error
            }

            // fill all professions for field
            professions.forEach((d: any) => {
                military[field].professions[d.en] = {
                    en: d.en,
                    he: d.he
                }
            })

            // write back to file
            fs.writeFileSync(__dirname + '/../../public/data/military.json', JSON.stringify(military))

            // get military data
            const militaryData = this.getMilitaryData()

            // send military data
            res.send(militaryData)
        } catch (err) { errorHandler.basicHandler(err) }
    }

    removeMilitaryProfession = async (req: Request, res: Response) => {
        try {
            const { field, profession }  = req.body

            // check if field exist in list
            if (!military[field]) {
                console.error('Field ' + military[field] + ' not exist in military')
                throw new Error
            }


            // check if military exist in list
            if (!military[field].professions[profession]) {
                console.error('Diploma ' + military[field].professions[profession] + ' not exist in military')
                throw new Error
            }

            // remove field from military list
            delete military[field].professions[profession]

            // write back to file
            fs.writeFileSync(__dirname + '/../../public/data/military.json', JSON.stringify(military))

            // get military data
            const militaryData = this.getMilitaryData()

            // send military data
            res.send(militaryData)
        } catch (err) { errorHandler.basicHandler(err) }
    }

    updateMilitaryProfession = (req: Request, res: Response) => {
        try {
            const { field, profession, newValue, lang } = req.body

            // check if field exist in list
            if (!military[field]) {
                console.error('Field ' + military[field] + ' not exist in military')
                throw new Error
            }

            // check if military exist in list
            if (!military[field].professions[profession]) {
                console.error('Diploma ' + military[field].professions[profession] + ' not exist in military')
                throw new Error
            }

            // update field in military list
            military[field].professions[profession][lang] = newValue

            // write back to file
            fs.writeFileSync(__dirname + '/../../public/data/military.json', JSON.stringify(military))

            res.end()
        } catch (err) { errorHandler.basicHandler(err) }
    }

    addMilitaryDepartments = async (req: Request, res: Response) => {
        try {
            const { field, departments } = req.body

            // check if field already exist in list
            if (!military[field]) {
                console.error('Field ' + military[field] + ' not exist in military')
                throw new Error
            }

            // fill all departments for field
            departments.forEach((d: any) => {
                military[field].departments[d.en] = {
                    en: d.en,
                    he: d.he
                }
            })

            // write back to file
            fs.writeFileSync(__dirname + '/../../public/data/military.json', JSON.stringify(military))

            // get military data
            const militaryData = this.getMilitaryData()

            // send military data
            res.send(militaryData)
        } catch (err) { errorHandler.basicHandler(err) }
    }

    removeMilitaryDepartment = async (req: Request, res: Response) => {
        try {
            const { field, department }  = req.body

            // check if field exist in list
            if (!military[field]) {
                console.error('Field ' + military[field] + ' not exist in military')
                throw new Error
            }


            // check if military exist in list
            if (!military[field].departments[department]) {
                console.error('Diploma ' + military[field].departments[department] + ' not exist in military')
                throw new Error
            }

            // remove field from military list
            delete military[field].departments[department]

            // write back to file
            fs.writeFileSync(__dirname + '/../../public/data/military.json', JSON.stringify(military))

            // get military data
            const militaryData = this.getMilitaryData()

            // send military data
            res.send(militaryData)
        } catch (err) { errorHandler.basicHandler(err) }
    }

    updateMilitaryDepartment = (req: Request, res: Response) => {
        try {
            const { field, department, newValue, lang } = req.body

            // check if field exist in list
            if (!military[field]) {
                console.error('Field ' + military[field] + ' not exist in military')
                throw new Error
            }

            // check if military exist in list
            if (!military[field].departments[department]) {
                console.error('Diploma ' + military[field].departments[department] + ' not exist in military')
                throw new Error
            }

            // update field in military list
            military[field].departments[department][lang] = newValue

            // write back to file
            fs.writeFileSync(__dirname + '/../../public/data/military.json', JSON.stringify(military))

            res.end()
        } catch (err) { errorHandler.basicHandler(err) }
    }


    /* COUNTRIES CITIES DATA */

    getCountriesCities = (req: Request, res: Response) => {
        try {
            // get language
            const lang = req.body.lang || 'en'

            const countriesCitiesData = this.getCountriesCitiesData(lang)

            res.send(countriesCitiesData)
        } catch (err) { errorHandler.basicHandler(err) }
    }

    getCountriesCitiesData (lang: string) {
        const countriesData = Object.entries(countries)
            .filter((c: any) => c[1][lang])
            .sort((a: any, b: any) => a[1][lang].toLowerCase().trim().localeCompare(b[1][lang].toLowerCase().trim()))

        countriesData.forEach((c: any) => {
            c[1].citiesList = Object.entries(c[1].cities)
                .filter((city: any) => city[1][lang])
                .sort((a: any, b: any) => a[1][lang].toLowerCase().trim().localeCompare(b[1][lang].toLowerCase().trim()))
        })
        return countriesData
    }
}


export default new RequirementController()