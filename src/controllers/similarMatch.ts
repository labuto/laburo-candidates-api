import { Request, Response } from 'express'
const { errorHandler, similarMatch } = require('../services')

class SimilarMatchController {

    get = async (req: Request, res: Response) => {
        try {
            // check if type exist
            if (!req.body.type) {
                errorHandler.basicHandler('similar match type is required in api /similarMatch/get')
            }

            // get similar match
            const response = await similarMatch.run(req.body.type)
            res.send(response)
        } catch (err) { errorHandler.basicHandler(err) }
    }

    add = async (req: Request, res: Response) => {
        try {
            const {type, matches} = req.body

            // check if type exist
            if (!type) {
                errorHandler.basicHandler('similar match type is required in api /similarMatch/add')
            }

            // add similar match
            const response = await similarMatch.run(type, matches)
            res.send(response)
        } catch (err) { errorHandler.basicHandler(err) }
    }

    update = async (req: Request, res: Response) => {
        try {
            const {type, match} = req.body

            // check if type exist
            if (!type) {
                errorHandler.basicHandler('similar match type is required in api /similarMatch/update')
            }

            // update similar match
            similarMatch.run(type, match)
            res.end()
        } catch (err) { errorHandler.basicHandler(err) }
    }

    remove = async (req: Request, res: Response) => {
        try {
            const {type, id} = req.body

            // check if type exist
            if (!type || !id) {
                errorHandler.basicHandler('similar match type/id is required in api /similarMatch/remove')
            }

            // remove similar match
            similarMatch.run(type, id)
            res.end()
        } catch (err) { errorHandler.basicHandler(err) }
    }
}

export default new SimilarMatchController()