const fs = require('fs')
const positions = JSON.parse(fs.readFileSync(__dirname + '/../../public/data/positions', 'utf8'))
const education = JSON.parse(fs.readFileSync(__dirname + '/../../public/data/education.json', 'utf8'))
const military = JSON.parse(fs.readFileSync(__dirname + '/../../public/data/military.json', 'utf8'))

import { Request, Response } from 'express'
const { errorHandler, textMatch } = require('../services')

class TextMatchController {

    get = async (req: Request, res: Response) => {
        try {
            // check if type exist
            if (!req.body.type) {
                errorHandler.basicHandler('text match type is required in api /textMatch/get')
            }

            // get text match
            const response = await textMatch.run(req.body.type)
            response.forEach((res: any) => {
                switch (req.body.type) {
                    case 'getPositions':
                        if (positions[res.field]) {
                            res.position_hebrew = positions[res.field].positions[res.position]
                                ? positions[res.field].positions[res.position].he
                                : ''
                        }
                        break
                    case 'getDiplomas':
                        if (education[res.field]) {
                            res.diploma_hebrew = education[res.field].diplomas[res.diploma]
                                ? education[res.field].diplomas[res.diploma].he
                                : ''
                        }
                        break
                    case 'getMilitaryProfession':
                        if (military[res.field]) {
                            res.profession_hebrew = military[res.field].professions[res.profession]
                                ? military[res.field].professions[res.profession].he
                                : ''
                        }
                        break
                }

            })
            res.send(response)
        } catch (err) { errorHandler.basicHandler(err) }
    }

    add = async (req: Request, res: Response) => {
        try {
            const {type, matches} = req.body

            // check if type exist
            if (!type) {
                errorHandler.basicHandler('text match type is required in api /textMatch/add')
            }

            // add similar match
            const response = await textMatch.run(type, matches)
            res.send(response)
        } catch (err) { errorHandler.basicHandler(err) }
    }

    update = async (req: Request, res: Response) => {
        try {
            const {type, match} = req.body

            // check if type exist
            if (!type) {
                errorHandler.basicHandler('text match type is required in api /textMatch/update')
            }

            // update similar match
            textMatch.run(type, match)
            res.end()
        } catch (err) { errorHandler.basicHandler(err) }
    }

    remove = async (req: Request, res: Response) => {
        try {
            const {type, id} = req.body

            // check if type, id exist
            if (!type || !id) {
                errorHandler.basicHandler('text match type/id is required in api /textMatch/remove')
            }

            // remove text match
            textMatch.run(type, id)
            res.end()
        } catch (err) { errorHandler.basicHandler(err) }
    }
}

export default new TextMatchController()