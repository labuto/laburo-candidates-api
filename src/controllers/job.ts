import { Request, Response } from 'express'
import {JobCandidateWatch} from '../entity/JobCandidateWatch'
import {JobCandidateRemove} from '../entity/JobCandidateRemove'
import {JobCandidateSent} from '../entity/JobCandidateSent'
import {Job} from '../entity'
const { errorHandler, typeOrm, validation, geolib } = require('../services')

class JobController {
    get = async (req: Request, res: Response) => {
        try {
            const { search, limit }: { search: any, limit: number } = req.body

            const parsedSearch = JSON.parse(search)

            const query: any = {
                text: `
                SELECT j.*, 
                c.id AS company_id, c.title as company_name, c.photo as company_photo, c.website as company_website, c.description as about_the_company,
                (SELECT json_agg(ja.*) FROM job_advertise as ja)  as job_advertise,
                COUNT(jcw.*) AS watch_counter 
                FROM jobs AS j               
                INNER JOIN companies c ON c.id = j.company_id               
                CROSS JOIN json_array_elements(j.positions) each_position
                WHERE 
                NOT j.is_deleted AND each_position->>'field' = $1                                        
            `,
                values: [parsedSearch.position.field]
            }

            query.text += ` GROUP BY j.id, c.id ORDER BY advertise_at DESC `

            // add limit to query
            if (limit)  {
                query.text += `LIMIT $${query.values.length + 1}`
                query.values.push(limit)
            }

            let jobs = (await typeOrm.q(query.text, query.values))

            jobs.forEach((job: any ) => {
                if (job.job_advertise) {
                    Object.assign(job, { advertise: job.job_advertise[0] })
                }
            })

            const sortedJob = jobs.sort((a: any, b: any) => a.distance - b.distance)
            res.send(sortedJob)
        } catch (err) { errorHandler.basicHandler('jobs/get/', err, res) }
    }

    getById = async (req: Request, res: Response) => {
        try {
            const { id }: { id: number } = req.body

            const query: any = {
                text: `
                SELECT j.*, 
                c.id AS company_id, c.title as company_name, c.photo as company_photo, c.website as company_website, c.description as about_the_company,
                (SELECT json_agg(ja.*) FROM job_advertise as jad)  as job_advertise,
                COUNT(jcw.*) AS watch_counter 
                FROM jobs AS j               
                INNER JOIN companies c ON c.id = j.company_id               
                CROSS JOIN json_array_elements(j.positions) each_position
                WHERE 
                NOT j.is_deleted AND j.status IN('open', 'inProcess') AND j.id = $1                              
                GROUP BY j.id, c.id               
                ORDER BY created_date
            `,
                values: [id]
            }

            let job = (await typeOrm.q(query.text, query.values))[0]

            if (job.job_advertise) {
                Object.assign(job, { advertise: job.job_advertise[0] })
            }
            res.send(job)
        } catch (err) { errorHandler.basicHandler('jobs/getById/', err, res) }
    }

    getHistory = async (req: Request, res: Response) => {
        try {
            const { filters, limit, candidateId }: { filters: any, limit: number, candidateId: number } = req.body

            const query: any = {
                text: `
                SELECT j.*, 
                c.id AS company_id, c.title as company_name, c.photo as company_photo, c.website as company_website, c.description as about_the_company,
                (SELECT json_agg(ja.*) FROM job_advertise as ja)  as job_advertise,
                COUNT(jcw.*) AS watch_counter,
                CASE WHEN COUNT(jcr.*) > 0 THEN 1 ELSE 0 END AS removed,
                CASE WHEN COUNT(jcs.*) > 0 THEN 1 ELSE 0 END AS sent 
                FROM jobs AS j
                INNER JOIN companies c ON c.id = j.company_id               
                WHERE NOT j.is_deleted AND j.status IN('open', 'inProcess') AND (jcr.candidate_id = $1 OR jcs.candidate_id = $1)
                GROUP BY j.id, c.id               
                ORDER BY created_date
            `,
                values: [candidateId]
            }


            // add limit to query
            if (limit)  {
                query.text += `LIMIT $3`
                query.values.push(limit)
            }

            let jobs = (await typeOrm.q(query.text, query.values))

            jobs.forEach((job: any ) => {
                if (job.job_advertise) {
                    Object.assign(job, { advertise: job.job_advertise[0] })
                }
            })
            res.send(jobs)
        } catch (err) { errorHandler.basicHandler('jobs/getHistory/', err, res) }
    }

    addCandidateWatch = async (req: Request, res: Response) => {
        try {
            const { jobId, companyId, candidateId } = req.body
            validation('addJobCandidate', { jobId, companyId, candidateId })

            const data = {
                job_id: jobId,
                company_id: companyId,
                candidate_id: candidateId
            }
            const alreadyWatched = await JobCandidateWatch.findOne(data)
            if (!alreadyWatched) {
                await JobCandidateWatch.create(data).save()
            }

            res.end()
        } catch (err) {
            errorHandler.basicHandler('jobs/addCandidateWatch/', err, res)
        }
    }

    addCandidateRemove = async (req: Request, res: Response) => {
        try {
            const { jobId, companyId, candidateId } = req.body
            validation('addJobCandidate', { jobId, companyId, candidateId })

            const data = {
                job_id: jobId,
                company_id: companyId,
                candidate_id: candidateId
            }
            const alreadyRemoved = await JobCandidateRemove.findOne(data)
            if (!alreadyRemoved) {
                await JobCandidateRemove.create(data).save()
            }

            res.end()
        } catch (err) {
            errorHandler.basicHandler('jobs/addCandidateRemove/', err, res)
        }
    }

    removeCandidateRemove = async (req: Request, res: Response) => {
        try {
            const { jobId, companyId, candidateId } = req.body
            validation('addJobCandidate', { jobId, companyId, candidateId })

            const data = {
                job_id: jobId,
                company_id: companyId,
                candidate_id: candidateId
            }
            await JobCandidateRemove.delete(data)

            res.end()
        } catch (err) {
            errorHandler.basicHandler('jobs/removeCandidateRemove/', err, res)
        }
    }

    addCandidateSent = async (req: Request, res: Response) => {
        try {
            const { jobId, companyId, candidateId } = req.body
            validation('addJobCandidate', { jobId, companyId, candidateId })

            const data = {
                job_id: jobId,
                company_id: companyId,
                candidate_id: candidateId
            }
            const alreadySent = await JobCandidateSent.findOne(data)
            if (!alreadySent) {
                await JobCandidateSent.create(data).save()
            }

            res.end()
        } catch (err) {
            errorHandler.basicHandler('jobs/addCandidateSent/', err, res)
        }
    }
}

export default new JobController()
