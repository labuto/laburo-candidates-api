import { Request, Response } from 'express'
import {Contact} from '../entity/Contact'
const { errorHandler, validation } = require('../services')

class ContactController {
    addMessage = async (req: Request, res: Response) => {
        try {
            const { contact } = req.body
            validation('addContactMessage', contact)

            await Contact.create(contact).save()

            res.end()
        } catch (err) {
            errorHandler.basicHandler('contact/addMessage', err, res)
        }
    }
}

export default new ContactController()