import { Request, Response } from 'express'
import { Candidate, CandidateLanguages, CandidateExperience, CandidateEducation, CandidateMilitary, CandidateKey, CandidateGeneral, CandidateJobSearch, CandidateJobShare } from '../entity/Candidate'
import {CandidateHobby} from '../entity/Candidate/CandidateHobby'
import {CandidateSkill} from '../entity/Candidate/CandidateSkill'
import {CandidateProfileComplete} from '../entity/Candidate/CandidateProfileComplete'
import {CandidateExtraFile} from '../entity/Candidate/CandidateExtraFile'
const { typeOrm, validation, errorHandler, CandidatesLib, bcryptService } = require('../services')

class CandidateController {

    // GET SINGLE
    getByEmail = async (req: Request, res: Response) => {
        try {
            const { email } = req.body

            // get candidates with basic data
            const candidate: Candidate | undefined = await Candidate.createQueryBuilder()
                .where('(email = :email) AND NOT is_deleted', { email })
                .getOne()

            res.send(candidate)
        } catch (err) { errorHandler.basicHandler('candidates/getByEmail', err, res) }
    }

    getBasicById = async (req: Request, res: Response) => {
        try {
            const { id } = req.body

            // get candidates with basic data
            const candidate: Candidate | undefined = await Candidate.findOne(id)

            res.send(candidate)
        } catch (err) { errorHandler.basicHandler(err) }
    }

    getFullById = async (req: Request, res: Response) => {
        try {
            // check if candidate id exist
            if (!req.body.id) {
                errorHandler.basicHandler('/candidates/getById', 'candidate id is required in api', res)
            }

            // get candidate with them languages, military, experience, job search and education data
            const query = {
                text: `
            SELECT e.id as employer_id, 
            c.*, 
            json_agg(cg.*) as general,
            json_agg(cpc.*) as profile_complete, 
            e.hide_process, 
            CASE WHEN COUNT(e.*) > 0 THEN 1 ELSE 0 END AS is_employer,            
            (SELECT json_agg(json_build_object('jobId', j.id, 'jobName', j.title, 'jobProcesses', j.processes, 'approveByManager', j.approve_by_manager)) FROM jobs as j WHERE j.company_id = $2) as jobs,           
            (SELECT json_agg(cl.*) FROM candidate_languages as cl WHERE cl.candidate_id = c.id)  as languages,
            (SELECT json_agg(cm.*) FROM candidate_military as cm WHERE cm.candidate_id = c.id)  as military,
            (SELECT json_agg(cex.* ORDER BY cex.to_date DESC) FROM candidate_experience as cex WHERE cex.candidate_id = c.id) as experience,
            (SELECT json_agg(cjs.*) FROM candidate_job_search as cjs WHERE cjs.candidate_id = c.id) as job_search,
            (SELECT json_agg(ced.*) FROM candidate_education as ced WHERE ced.candidate_id = c.id) as education,
            (SELECT json_agg(cho.*) FROM candidate_hobbies as cho WHERE cho.candidate_id = c.id) as hobbies,
            (SELECT json_agg(csk.*) FROM candidate_skills as csk WHERE csk.candidate_id = c.id) as skills,
            (SELECT json_agg(cef.*) FROM candidate_extra_files as cef WHERE cef.candidate_id = c.id) as extra_files                       
            FROM candidates as c
            LEFT JOIN candidate_general AS cg ON c.id = cg.candidate_id
            LEFT JOIN candidate_profile_complete AS cpc ON c.id = cpc.candidate_id           
            LEFT JOIN jobProcess AS jp ON c.id = jp.candidate_id
            LEFT JOIN employers AS e ON c.id = e.candidate_id AND e.company_id = $2           
            WHERE c.id = $1 AND NOT c.is_deleted                       
            GROUP BY e.id, c.id, e.hide_process
            ORDER BY e.id DESC
        `,
                values: [req.body.id, req.body.companyId]
            }
            const candidate = (await typeOrm.q(query.text, query.values))[0]
            if (!candidate) {
                res.status(405).send(`Candidate doesn't exist`)
                return
            }

            if (candidate.job_search) {
                Object.assign(candidate, { job_search: candidate.job_search[0] })
            }

            if (candidate.profile_complete) {
                Object.assign(candidate, { profile_complete: candidate.profile_complete[0] })
            }

            res.send(candidate)
        } catch (err) { errorHandler.basicHandler('candidates/getFullById', err, res) }
    }

    getJobSearchById = async (req: Request, res: Response) => {
        try {
            // check if candidate id exist
            validation('checkIdExist', { id: req.body.id })

            // get candidate with them languages, military, experience, job search and education data
            const query = {
                text: `
                    SELECT c.*, 
                    json_agg(cg.*) as general,                         
                    (SELECT json_agg(cjs.*) FROM candidate_job_search as cjs WHERE cjs.candidate_id = c.id) as job_search           
                    FROM candidates as c
                    LEFT JOIN candidate_general AS cg ON c.id = cg.candidate_id                       
                    WHERE c.id = $1 AND NOT c.is_deleted
                    GROUP BY c.id           
                `,
                values: [req.body.id]
            }

            const candidate = (await typeOrm.q(query.text, query.values))[0]
            if (!candidate) {
                res.status(405).send(`Candidate doesn't exist`)
                return
            }

            if (candidate.job_search) {
                Object.assign(candidate, { job_search: candidate.job_search[0] })
            }

            res.send(candidate)
        } catch (err) { errorHandler.basicHandler('candidates/getJobSearchById', err, res) }
    }



    // ADD/UPDATE
    add = async (req: Request, res: Response) => {
        try {
            const { candidate, password } = req.body

            validation('candidate', candidate)

            const existCandidate: Candidate | undefined = await Candidate.createQueryBuilder()
                .where('(email = :email) AND NOT is_deleted', { email: candidate.email })
                .getOne()

            if (existCandidate) {
                if (existCandidate.source === 'openLink' || existCandidate.source === 'candidate') {
                    res.status(405).send('Candidate Already Exist')
                    return
                }

                await existCandidate.remove()
            }

            // add new candidate
            const data: object = {
                full_name: candidate.full_name,
                email: candidate.email,
                privacy: 'public',
                companies: '[]',
                not_companies: '[]',
                source: 'candidates'
            }
            const newCandidate = await Candidate.create(data).save()

            const hash: string = newCandidate.id + password + new Date(newCandidate.created_date).getTime()
            newCandidate.hash = bcryptService.myEncrypt(hash)

            await newCandidate.save()

            await CandidateGeneral.create({ candidate_id: newCandidate.id }).save()

            await CandidateProfileComplete.create({ candidate_id: newCandidate.id }).save()

            await CandidateJobSearch.create({ candidate_id: newCandidate.id }).save()

            res.send(newCandidate)
        } catch (err) { errorHandler.basicHandler('/candidates/add', err, res) }
    }

    changePassword = async (req: Request, res: Response) => {
        try {
            const { id, password } = req.body

            validation('candidateChangePassword', { id, password })

            const candidate: Candidate | undefined = await Candidate.findOne(id)
            if (candidate) {
                const hash: string = id + password + new Date(candidate.created_date).getTime()
                candidate.hash = bcryptService.myEncrypt(hash)

                candidate.hash = hash
                await candidate.save()
            }

            res.send(candidate)
        } catch (err) { errorHandler.basicHandler('/candidates/changePassword', err, res) }
    }

    updateSingleData = async (req: Request, res: Response) => {
        try {
            const { candidateId, type, value } = req.body

            const existCandidate = await Candidate.findOne(candidateId)
            if (!existCandidate) {
                res.status(405).send('Candidate Not Exist')
                return
            }

            if (type === 'location' || type === 'photo') {
                await CandidateGeneral.update({ candidate_id: candidateId }, { [type]: value })
            } else {
                await Candidate.update(candidateId, { [type]: value })
            }

            res.end()
        } catch (err) { errorHandler.basicHandler('candidates/updateSingleData', err, res) }
    }

    updateGeneral = async (req: Request, res: Response) => {
        try {
            const { candidateId, general } = req.body

            const existCandidate = await Candidate.findOne(candidateId)
            if (!existCandidate) {
                res.status(405).send('Candidate Not Exist')
                return
            }

            Object.assign(general, { birthday: new Date(general.birthday).getTime() })
            await CandidateGeneral.update({ candidate_id: candidateId }, general)

            const completeGeneral = general.phone && general.birthday && general.family_status && general.gender && general.about_me
            await CandidateProfileComplete
                .update({ candidate_id: candidateId }, { general: completeGeneral ? true : false })

            res.end()
        } catch (err) { errorHandler.basicHandler('candidates/updateGeneral', err, res) }
    }

    updateAchievement = async (req: Request, res: Response) => {
        try {
            const { candidateId, achievement, type } = req.body

            const existCandidate = await Candidate.findOne(candidateId)
            if (!existCandidate) {
                res.status(405).send('Candidate Not Exist')
                return
            }

            let response: any
            const candidatesLib = new CandidatesLib(candidateId)
            switch (type) {
                case 'experience':
                    validation('candidateExperience', achievement)
                    await candidatesLib.candidateExperience({ experience: [achievement] })
                    response = await CandidateExperience.find({
                        where: { candidate_id: candidateId },
                        order: {
                            to_date: 'DESC'
                        }
                    })

                    if (!achievement.id) {
                        await CandidateProfileComplete.update({ candidate_id: candidateId }, { experience: true })
                    }

                    break
                case 'language':
                    validation('candidateLanguages', achievement)
                    candidatesLib.candidateLanguages({ languages: [achievement] })
                    break
                case 'education':
                    validation('candidateEducation', achievement)
                    await candidatesLib.candidateEducation({ education: [achievement] })
                    response = await CandidateEducation.find({
                        where: { candidate_id: candidateId },
                        order: {
                            to_date: 'DESC'
                        }
                    })

                    if (!achievement.id) {
                        await CandidateProfileComplete.update({ candidate_id: candidateId }, { education: true })
                    }

                    break
                case 'military':
                    validation('candidateMilitary', achievement)
                    await candidatesLib.candidateMilitary({ military: [achievement] })
                    response = await CandidateMilitary.find({
                        where: { candidate_id: candidateId },
                        order: {
                            to_date: 'DESC'
                        }
                    })
                    if (!achievement.id) {
                        await CandidateProfileComplete.update({ candidate_id: candidateId }, { military: true })
                    }

                    break
                case 'hobby':
                    validation('candidateHobby', achievement)
                    const hobbyData = { value: achievement.value, candidate_id: candidateId }
                    achievement.id
                        ? await CandidateHobby.update(achievement.id, hobbyData)
                        : await CandidateHobby.create(hobbyData).save()

                    response = await CandidateHobby.find({
                        where: { candidate_id: candidateId },
                        order: {
                            id: 'ASC'
                        }
                    })

                    break
                case 'skill':
                    validation('candidateSkill', achievement)
                    const skillData = { value: achievement.value, candidate_id: candidateId }
                    achievement.id
                        ? await CandidateSkill.update(achievement.id, skillData)
                        : await CandidateSkill.create(skillData).save()

                    response = await CandidateSkill.find({
                        where: { candidate_id: candidateId },
                        order: {
                            id: 'ASC'
                        }
                    })

                    if (!achievement.id) {
                        await CandidateProfileComplete.update({ candidate_id: candidateId }, { skills: true })
                    }

                    break
                case 'extraFiles':
                    validation('candidateExtraFiles', achievement)
                    response = await CandidateExtraFile.create(achievement).save()
                    break
            }

            res.send(response)
        } catch (err) { errorHandler.basicHandler('candidates/updateAchievement', err, res) }
    }

    removeAchievement = async (req: Request, res: Response) => {
        try {
            const { achievementId, candidateId, type } = req.body

            validation('removeCandidateAchievement', { id: achievementId })

            switch (type) {
                case 'experience':
                    await CandidateExperience.delete(achievementId)
                    const hasExperience = await CandidateExperience.find({ candidate_id: candidateId })
                    if (!hasExperience.length){
                        await CandidateProfileComplete.update({ candidate_id: candidateId }, { experience: false })
                    }
                    break
                case 'language':
                    await CandidateLanguages.delete(achievementId)
                    break
                case 'education':
                    await CandidateEducation.delete(achievementId)

                    const hasEducation = await CandidateEducation.find({ candidate_id: candidateId })
                    if (!hasEducation.length){
                        await CandidateProfileComplete.update({ candidate_id: candidateId }, { education: false })
                    }
                    break
                case 'military':
                    await CandidateMilitary.delete(achievementId)
                    break
                case 'hobby':
                    await CandidateHobby.delete(achievementId)
                    break
                case 'skill':
                    await CandidateSkill.delete(achievementId)

                    const hasSkills = await CandidateSkill.find({ candidate_id: candidateId })
                    if (!hasSkills.length){
                        await CandidateProfileComplete.update({ candidate_id: candidateId }, { skills: false })
                    }
                    break
                case 'extraFiles':
                    await CandidateExtraFile.delete(achievementId)
                    break
            }

            res.end()
        } catch (err) { errorHandler.basicHandler('candidates/removeAchievement', err, res) }
    }

    updateJobSearch = async (req: Request, res: Response) => {
        try {
            const { candidateId, jobSearch } = req.body

            const existCandidate = await Candidate.findOne(candidateId)
            if (!existCandidate) {
                res.status(405).send('Candidate Not Exist')
                return
            }

            validation('updateCandidateJobSearch', { position: jobSearch.position })
            const candidateLib = new CandidatesLib(candidateId)
            candidateLib.candidateJobSearch({ jobSearch })

            res.end()
        } catch (err) { errorHandler.basicHandler('candidates/updateJobSearch', err, res) }
    }

    removeJobSearch = async (req: Request, res: Response) => {
        try {
            const { jobSearchId } = req.body

            validation('removeCandidateJobSearch', { id: jobSearchId })
            await CandidateJobSearch.delete(jobSearchId)

            res.end()
        } catch (err) { errorHandler.basicHandler('candidates/removeJobSearch', err, res) }
    }

    updateShare = async (req: Request, res: Response) => {
        try {
            const { candidateId, jobId, emails } = req.body

            validation('updateCandidateShare', { candidateId, jobId })

            if (!emails || !emails.length) {
                res.end()
                return
            }

            const existCandidate = await Candidate.findOne(candidateId)
            if (!existCandidate) {
                res.status(405).send('Candidate Not Exist')
                return
            }

            const emailData: Array<any> = emails.map((e: string) => ({ candidate_id: candidateId, job_id: jobId, share_email: e }))

            await CandidateJobShare
                .createQueryBuilder()
                .insert()
                .values(emailData)
                .execute()

            res.end()
        } catch (err) { errorHandler.basicHandler('candidates/updateShare', err, res) }
    }



    // CHECK
    checkCandidateExist = async (req: Request, res: Response) => {
        try {
            const { type, value } = req.body

            // validation
            if (type !== 'email') {
                errorHandler.basicHandler('/candidates/checkCandidateExist', 'type must be email in api', res)
            }

            const candidate = await Candidate.createQueryBuilder()
                .where(`${type} = :${type} AND (source = 'openLink' OR source = 'candidates')`, { [type]: value })
                .getOne()

            // add company id if exist
            if (candidate) {
                res.send({ candidateId: candidate.id })
                return
            }

            res.end()
        } catch (err) {
            errorHandler.basicHandler(err)
        }
    }


    // VALIDATION KEY
    addCandidateKey = async (req: Request, res: Response) => {
        try {
            validation('addCandidateKey', req.body)

            const { email, key } = req.body

            // add key
            await CandidateKey.delete({ candidate_email: email })
            await CandidateKey.create({ key, candidate_email: email })

            res.end()
        } catch (err) { errorHandler.basicHandler('candidates/addCandidateKey', err, res) }
    }

    removeCandidateKey = async (req: Request, res: Response) => {
        try {
            validation('removeCandidateKey', req.body)

            const { email } = req.body

            // remove key
            await CandidateKey.delete({ candidate_email: email })

            res.end()
        } catch (err) { errorHandler.basicHandler('candidates/removeCandidateKey', err, res) }
    }

    validCandidateKey = async (req: Request, res: Response) => {
        try {
            validation('addCandidateKey', req.body)

            const { email, key } = req.body

            // check if key in user key
            const keys = await CandidateKey.find({ candidate_email: email, key })
            const valid = keys.length > 0

            res.send(valid)
        } catch (err) { errorHandler.basicHandler('candidates/validCandidateKey', err, res) }
    }

    updateResumeName = async (req: Request, res: Response) => {
        try {
            validation('updateResumeName', req.body)

            const { candidateId, fileName } = req.body

            await CandidateGeneral.update({ candidate_id: candidateId }, { resume: fileName })

            res.end()
        } catch (err) { errorHandler.basicHandler('candidates/updateResumeName', err, res) }
    }
}

export default new CandidateController()

