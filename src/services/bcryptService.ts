const bcrypt = require('bcryptjs')


function myEncrypt(data: any) {
  const salt = bcrypt.genSaltSync(10)
  const hash: string = data

  const hashPassword: string = bcrypt.hashSync(hash, salt)
  return hashPassword
}

function myCompare (compare: string, hash: string) {
  return bcrypt.compareSync(compare, hash)
}

export default { myEncrypt, myCompare }
