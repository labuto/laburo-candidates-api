const pg = require('pg')
const format = require('pg-format')

interface ConnectConfig {
    usr: string | undefined;
    pwd: string | undefined;
    dbname: string | undefined;
    endpoint: string | undefined;
}

interface Query {
    text: string,
    values: Array<any>
}

class DbService {
    protected SLOW_CONNECTION_WARNING: string = `Connection to DB is taking too long... `

    // connect to local or server database
    protected config: ConnectConfig = {
        usr: process.env.DB_usr,
        pwd: process.env.DB_pwd,
        dbname: process.env.DB_dbname,
        endpoint: process.env.DB_endpoint
    }

    private readonly client: any
    private isConnected: boolean = false

    constructor () {
        this.client = new pg.Client(`pg://${this.config.usr}:${this.config.pwd}@${this.config.endpoint}/${this.config.dbname}`)
    }

    getClient = () => { return this.client }

    init: any = async () => {
        console.log('DBService > Connecting to db')
        setTimeout(() => !this.isConnected && console.warn(this.SLOW_CONNECTION_WARNING), 2000)
        await this.client.connect()
        this.isConnected = true

        /*
        CREATE RULE shoe_del_protect AS ON DELETE TO shoe
        DO INSTEAD NOTHING;
        https://www.postgresql.org/docs/9.1/rules-update.html

        DROP RULE shoe_del_protect ON shoe
        https://www.postgresql.org/docs/current/sql-droprule.html
        */


        // await dropTable()

        const createTableQuery = ``
        await this.q(createTableQuery)
    }

    // build format
    formatQ: any = (str: Query) => {
        try {
            const query = format(str.text, str.values)
            return query
        } catch (err) {
            console.error(err.message + ' format in query:\n' + str.text + '\nvalues \n' + JSON.stringify(str.values))
            throw new Error
        }
    }


    q: any = async (str: string | Query) => {
        const res = await this.client.query(str)
            .catch((err: any) => {
                err.message += ' in query:\n' + str
                throw err
            })
        return res
    }

    // drop all table in database
    dropTable: any = async () => {
        const query: string = `
            SELECT 'drop table if exists "' || tablename || '" cascade;' as pg_drop
            FROM pg_tables
            WHERE schemaname='public';
        `
        const tableDeleteQs = (await this.q(query)).rows
        tableDeleteQs.forEach((deleteQ: any) => {
            this.q(deleteQ.pg_drop)
        })
    }
}

export default new DbService()
