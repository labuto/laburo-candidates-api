import dbService from './dbService'
import typeOrm from './typeOrm'
import cryptoJs from './cryptoJs'
import Listener from './listener'
import validation from './validation'
import errorHandler from './errorHandler'
import CandidatesLib from './candidatesLib'
import similarMatch from './systemLearning/similarMatch'
import textMatch from './systemLearning/textMatch'
import bcryptService from './bcryptService'

module.exports = {
    dbService,
    typeOrm,
    cryptoJs,
    Listener,
    validation,
    errorHandler,
    fileUploader: require('./fileUploader'),
    generatePassword: require('generate-password'),
    geolib: require('geolib'),
    CandidatesLib,
    similarMatch,
    textMatch,
    bcryptService
}
