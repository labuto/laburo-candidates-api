const basicHandler = (path: string, err: any, res: any) => {
  console.error(err)
  if (!err.response) {
    err = err.message
  }

  err.response
    ? console.error(`\nError in ${path}` + '\n' + err + '\n' + err.response.data + '\n')
    : console.error(`\nError in ${path}` + '\n' + err + '\n')

  const status = err.response ? err.response.status : 422
  const data = err.response ? err.response.data : err

  res.status(status).send(data)
}

const validationHandler = (err: any) => {
  const newErr = {
    response: {
      status: 422,
      data: 'Error In Validation:\n ' + JSON.stringify(err)
    }
  }

  throw Error(JSON.stringify(newErr))
}

export default { basicHandler, validationHandler }
