import fs from 'fs'
const schema = JSON.parse(fs.readFileSync(__dirname + '/schema.json', 'utf8'))
import errorHandler from '../errorHandler'

const Ajv = require('ajv')
const ajv = new Ajv({
    allErrors: true,
    jsonPointers: false,
    coerceTypes: true,
    validateSchema: false,
    $data: true
})

export default (type: string, data: any) => {
    const validate = ajv.compile(schema[type])

    const valid = validate(data)

    if (!valid) { errorHandler.validationHandler(validate.errors) }

    return valid
}
