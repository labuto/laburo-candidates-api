import fs from 'fs'
const filePath: string = __dirname + '/../../public/data/'

class FileUploader {
  static makeDir (name: string) {
    fs.mkdir(`${filePath}${name}`, (err) => {
      if (err && err.code !== 'EEXIST') {
        console.error(err)
      }
    })
  }

  static async move (file: any, to: string) {
    await file.mv(`${filePath}${to}`, (err: any) => {
      if (!err) {
        return `${filePath}${to}`
      }
      console.error(err)
    })
    return `${filePath}${to}`
  }

  static async copy (from: string, to: string) {
    await fs.rename(`${filePath}${from}`, `${filePath}${to}`, (err: any) => {
      if (!err) {
        return `${filePath}${to}`
      }
      console.error(err)
    })
    return `${filePath}${to}`
  }
}

module.exports = FileUploader
