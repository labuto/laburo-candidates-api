import {
    CandidateEducation, CandidateExperience, CandidateGeneral, CandidateJobSearch,
    CandidateLanguages, CandidateMilitary
} from '../entity/Candidate'
import {NewDataValue} from '../entity/NewDataValue'

import typeOrm from './typeOrm'
const errorHandler = require('./errorHandler')

class CandidateLib {
    private readonly candidateId: number

    constructor (candidateId: number) {
        this.candidateId = candidateId
    }

    getAll:any = async ({ isDeleted = false, approved = true }: { isDeleted: boolean, approved: boolean }) => {
        try {
            const query = {
                text: `
                    SELECT c.id,
                    (SELECT json_agg(cg.*) FROM candidate_general as cg WHERE cg.candidate_id = c.id)  as general,
                    (SELECT json_agg(cl.*) FROM candidate_languages as cl WHERE cl.candidate_id = c.id)  as languages,
                    (SELECT json_agg(cm.*) FROM candidate_military as cm WHERE cm.candidate_id = c.id)  as military,
                    (SELECT json_agg(cex.*) FROM candidate_experience as cex WHERE cex.candidate_id = c.id)  as experience,
                    (SELECT json_agg(cjs.*) FROM candidate_job_search as cjs WHERE cjs.candidate_id = c.id)  as job_search,
                    (SELECT json_agg(ced.*) FROM candidate_education as ced WHERE ced.candidate_id = c.id)  as education 
                     FROM candidates as c                                            
                     WHERE c.is_deleted = $1 AND c.approved $2
                     GROUP BY c.id
                     ORDER BY c.id desc
                `,
                values: [isDeleted, approved]
            }

            // get candidates with them languages, military, experience, job search and education data
            const candidates = await typeOrm.q(query.text, query.values)

            return candidates
        } catch (err) {
            errorHandler.basicHandler(err)
        }
    }

    getAllInProcesses: any = async ({ userId, companyId }: { userId: number, companyId: number }) => {
        try {
            // get candidates in job process in company
            const query = {
                text: `
                SELECT
                    j.*,
                    json_agg(
                    jsonb_build_object('process', jp.*, 'candidate_id', c.id, 'candidate_name', c.full_name, 'candidate_photo', cg.photo)                   
                    ) as candidates_processes 
                FROM jobs as j
                INNER JOIN job_process as jp ON jp.job_id = j.id AND jp.company_id = $1
                INNER JOIN candidates AS c ON c.id = jp.candidate_id
                LEFT JOIN candidate_general AS cg ON c.id = cg.candidate_id
                INNER JOIN user_company as uc ON uc.company_id = j.company_id AND (uc.user_id = $2 OR uc.parent_id = $2)                               
                WHERE                      
                    (j.created_id = $2 OR j.created_id = uc.user_id OR uc.permission = 'master')                
                GROUP BY j.id                                                  
                `,
                values: [companyId, userId]
            }

            const candidates = await typeOrm.q(query.text, query.values)

            return candidates
        } catch (err) {
            errorHandler.basicHandler('', err, {})
        }
    }

    candidateGeneral: any = async ({ general, newRecord = true }: { general: any, newRecord: boolean }) => {
        const data = {
            candidate_id: this.candidateId,
            photo: general.photo,
            resume: general.resume,
            phone: general.phone,
            birthday: general.birthday ? new Date(general.birthday) : undefined,
            family_status: general.family_status,
            location: general.location || {},
            gender: general.gender,
            about_me: general.about_me,
            extra_files: general.extra_files || []
        }

        await CandidateGeneral.create(data).save()
    }

    candidateLanguages: any = async ({ languages }: { languages: any }) => {
        // check and add/merge languages
        for (let i =0; i < languages.length; i++) {
            const l = languages[i]
            if (l.key && l.value) {
                const data = {
                    candidate_id: this.candidateId,
                    key: l.key,
                    value: l.value
                }

                await CandidateLanguages.create(data).save()
            }
        }
    }

    candidateJobSearch: any = async ({ jobSearch }: { jobSearch: any }) => {
        // add candidate job search
        if (jobSearch) {
            // check entered new position
            this.enteredNewValue({ value: jobSearch.position })

            const data = {
                candidate_id: this.candidateId,
                position: jobSearch.position || {},
                company_types: jobSearch.company_types,
                location: jobSearch.location || {},
                job_scope: jobSearch.job_scope,
                work_availability: jobSearch.work_availability,
                salary: parseFloat(jobSearch.salary || 0)
            }

            await CandidateJobSearch.create(data).save()
        }
    }

    candidateExperience: any = async ({ experience }: { experience: any }) => {
        // check and add/merge experience
        for (let i = 0; i < experience.length; i++) {
            const e = experience[i]
            if (!e.positions) {
                console.error('candidate experience positions is required')
                return
            }

            const data = {
                candidate_id: this.candidateId,
                positions: e.positions,
                company_type: e.company_type,
                company_name: e.company_name,
                salary: e.salary || undefined,
                location: e.location || {},
                job_scope: e.job_scope,
                job_likes: e.job_likes,
                job_dislikes: e.job_dislikes,
                from_date: e.from_date || undefined,
                to_date: e.to_date || undefined,
                description: e.description,
                recommend: e.recommend || {},
                computer_systems: e.computer_systems || [],
                comparison_obj: e.comparison_obj || {}
            }

            await CandidateExperience.create(data).save()

            // check entered new position
            this.enteredNewValue({ value: e.positions })
            this.enteredNewValue({ value: e.location })
        }
    }

    candidateEducation: any = async ({ education }: { education: any }) => {
        // check and add/merge education
        for (let i = 0; i < education.length; i++) {
            const e = education[i]
            if (!e.diploma_type) {
                console.error('candidate education diploma_type is required')
                return
            }

            const data = {
                candidate_id: this.candidateId,
                diploma_type: e.diploma_type,
                diplomas: e.diplomas,
                institute: e.institute,
                from_date: e.from_date || undefined,
                to_date: e.to_date || undefined,
                grade: e.grade || undefined,
                description: e.description,
                comparison_obj: e.comparison_obj || {}
            }

            await CandidateEducation.create(data).save()

            // check entered new diploma
            this.enteredNewValue({ value: e.diplomas })
            this.enteredNewValue({ value: e.institute, parent: e.diplomas })
        }
    }

    candidateMilitary: any = async ({ military }: { military: any }) => {
        // check and add/merge military
        for (let i = 0; i < military.length; i++) {
            const m = military[i]

            if (!m.professions) {
                console.error('candidate military profession is required')
                return
            }

            const data = {
                candidate_id: this.candidateId,
                professions: m.professions,
                comparison_obj: m.comparison_obj,
                unit: m.unit,
                rank: m.rank,
                from_date: m.from_date || undefined,
                to_date: m.to_date || undefined,
                description: m.description
            }

            await CandidateMilitary.create(data).save()

            // check entered new professions
            this.enteredNewValue({ value: m.professions })
        }
    }

    enteredNewValue: any = async ({ value, parent }: { value: any, parent: any }) => {
        if (!value.type) { return }

        const data: any = {
            type: value.type,
            candidate_id: this.candidateId,
            value: ''
        }

        switch (value.type) {
            case 'jobPosition':
                Object.assign(data, { value: value.positions[0] })
                break
            case 'educationField':
                Object.assign(data, { value: value.diplomas[0], parent: value.parent })
                break
            case 'institute':
                Object.assign(data, { value: value.institute, parent: JSON.stringify(parent) })
                break
            case 'militaryProfession':
                Object.assign(data, { value: value.professions[0] })
                break
            case 'languages':
                Object.assign(data, { value: value.key })
                break
            case 'location':
                Object.assign(data, { value: value.city, parent: value.country })
                break
        }

        const newValue = await NewDataValue.findOne({ type: value.type, value: data.value })
        newValue
            ? NewDataValue.update(newValue.id, data)
            : NewDataValue.create(data).save()
    }
}

export default CandidateLib
