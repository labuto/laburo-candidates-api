const fs = require('fs')
const format = require('pg-format')
const config = JSON.parse(fs.readFileSync(__dirname + '/../../ormconfig.json'))

interface Query {
    text: string,
    values: Array<any>
}

import "reflect-metadata"
import { createConnection, getManager, getConnection } from 'typeorm'

const init = async () => {
    try {
        await createConnection(config)
        console.log('Type Orm Connected')
    } catch(error) { console.log(error) }

}

const q = async (query: string, values: Array<any>) => {
    try {
        const entity = getManager()
        const rows = await entity.query(query, values)
        return rows
    } catch (err) {
        err.message += ' in query:\n' + query
        console.error(err)
        throw err
    }
}

// build format
const formatQ: any = (str: Query) => {
    try {
        const query = format(str.text, str.values)
        return query
    } catch (err) {
        console.error(err.message + ' format in query:\n' + str.text + '\nvalues \n' + JSON.stringify(str.values))
        throw new Error
    }
}

export default { init, q, getConnection, formatQ }
