const listeners: any = {}
const usersListener: any = {}

class Listener {
  private socket: any
  private socketId: number
  private queue: Array<any>
  private updateInterval: number
  private chunkSize: number
  private interval: any

  constructor (socket: any) {
    Object.assign(this, {
      socket: socket,
      socketId: socket.client.id,
      queue: [],
      updateInterval: 100,
      chunkSize: 50,
      interval: null,
    })

    if (listeners[this.socketId] !== undefined)
      throw Error(`listener with socket id: ${this.socketId} already exist`)

    listeners[this.socketId] = this
    console.log(`Listener opened with socket id: ${this.socketId}`)
    console.log(`Opened Listeners: ${Object.keys(listeners).length}`)
  }

  static setUserSocket (user: number, socketId: number) {
    usersListener[user] = socketId
  }

  static getListenerByUser (user: number) {
    return listeners[usersListener[user]]
  }

  static getListenerBySocket (socketId: number) {
    return listeners[socketId]
  }

  distribute ({ results, actionType, target }: { results: Array<any>, actionType: string, target: number }) {
    this.queue.push(...results)
    if (!this.interval){
      this.interval = setInterval(this._consumeQueue.bind({that: this, actionType, target}), this.updateInterval)
    }
  }

  _consumeQueue () {
    /* ;(async ({ that, actionType, target }: { that: any, actionType: string, target: number }) => {
      that.sendImmediate(actionType, [])
    })() */
  }

  sendImmediate (event: string, data: Array<any>) {
    this.socket.emit(event, data)
  }

  // invoked when socket hangs out
  destroy () {
    clearInterval(this.interval)
    delete listeners[this.socketId]
    console.log(`Listener closed with socket id: ${this.socketId}`)
    console.log(`Opened Listeners: ${Object.keys(listeners).length}`)
  }
}

export default Listener
