const cryptoJS = require('crypto-js')

interface Config {
    keySize: number,
    ivSize: number,
    iterations: number,
    password: string | undefined
}

const config: Config = {
    keySize: 256,
    ivSize: 128,
    iterations: 100,
    password: process.env.cryptoPass
}

function myEncrypt(data: any) {
    const salt = cryptoJS.lib.WordArray.random(config.ivSize / 8)

    const key = cryptoJS.PBKDF2(config.password, salt, {
        keySize: config.keySize/32,
        iterations: config.iterations
    })

    const iv = {
        iv: cryptoJS.lib.WordArray.random(config.ivSize / 8),
        padding: cryptoJS.pad.Pkcs7,
        mode: cryptoJS.mode.CBC
    }
    const encrypted = cryptoJS.AES.encrypt(data, key, iv)

    // salt, iv will be hex 32 in length
    // append them to the ciphertext for use  in decryption
    return salt.toString()+ iv.iv.toString() + encrypted.toString()
}

function myDecrypt(data: string){
    const salt = cryptoJS.enc.Hex.parse(data.substr(0, 32))
    const encrypted = data.substring(64);

    const key = cryptoJS.PBKDF2(config.password, salt, {
        keySize: config.keySize/32,
        iterations: config.iterations
    });

    const iv = {
        iv: cryptoJS.enc.Hex.parse(data.substr(32, 32)),
        padding: cryptoJS.pad.Pkcs7,
        mode: cryptoJS.mode.CBC
    }

    const decrypted = cryptoJS.AES.decrypt(encrypted, key, iv)
    return decrypted.toString(cryptoJS.enc.Utf8)
}

export default { myEncrypt, myDecrypt }
