const { qb } = require('../index')
import typeorm from '../typeOrm'

async function run(type: string, matches: any) {
    // run function
    const response: any = await funcs[type](matches)
    return response
}

// set up the possible functions:
const funcs: any = {
    getPositions: async () => {
        // get positions match
        const response = await typeorm.q(`SELECT * FROM textPositionMatch WHERE deleted_date IS NULL ORDER BY id`, [])
        return response
    },
    updatePosition: async (match: any) => {
        // update position match
        const where = [{ key: 'id', value: match.id }]
        const data = { text: match.text, counter: match.counter }
        await qb.update({ table: 'textPositionMatch', data, where })
    },
    removePosition: async (id: number) => {
        // remove position match
        const where = [{ key: 'id', value: id }]
        const data = { deleted_date: Date.now() }
        await qb.update({ table: 'textPositionMatch', data, where })

        // await qb.remove({ table: 'textPositionMatch', where: [{ key: 'id', value: id }] })
    },
    position: async (matches: any) => {
        // m = { text, field, position, counter }
        const response = []
        for (let i = 0; i < matches.length; i++) {
            const m = matches[i]

            // find if match exist
            const where = [
                { key: 'text', value: m.text },
                { key: 'field', value: m.field },
                { key: 'position', value: m.position }
            ]
            const match = await qb.read({ table: 'textPositionMatch', where })

            // update counter
            const rowCounter = match[0] ? match[0].counter : 0
            m.counter = parseInt(rowCounter) + parseInt(m.counter)

            // if exist, update. if not, add
            const row = match[0]
                ? await qb.update({ table: 'textPositionMatch', data: m, where: [{ key: 'id', value: match[0].id }] })
                : await qb.insert('textPositionMatch', m)

            response.push(row)
        }
        return response
    },
    getDiplomas: async () => {
        // get diplomas match
        const response = await typeorm.q(`SELECT * FROM textDiplomaMatch WHERE deleted_date IS NULL ORDER BY id`, [])
        return response
    },
    updateDiploma: async (match: any) => {
        // update diplomas match
        const where = [{ key: 'id', value: match.id }]
        const data = { text: match.text, counter: match.counter }
        await qb.update({ table: 'textDiplomaMatch', data, where })
    },
    removeDiploma: async (id: number) => {
        // remove diplomas match
        const where = [{ key: 'id', value: id }]
        const data = { deleted_date: Date.now() }
        await qb.update({ table: 'textDiplomaMatch', data, where })

        // await qb.remove({ table: 'textDiplomaMatch', where: [{ key: 'id', value: id }] })
    },
    diploma: async (matches: any) => {
        // m = { text, field, diploma, counter }
        const response = []
        for (let i = 0; i < matches.length; i++) {
            const m = matches[i]

            // find if match exist
            const where = [
                { key: 'text', value: m.text },
                { key: 'field', value: m.field },
                { key: 'diploma', value: m.diploma }
            ]
            const match = await qb.read({ table: 'textDiplomaMatch', where })

            // update counter
            const rowCounter = match[0] ? match[0].counter : 0
            m.counter = parseInt(rowCounter) + parseInt(m.counter)

            // if exist, update. if not, add
            const row = match[0]
                ? await qb.update({ table: 'textDiplomaMatch', data: m, where: [{ key: 'id', value: match[0].id }] })
                : await qb.insert('textDiplomaMatch', m)

            response.push(row)
        }
        return response
    },
    getMilitaryProfession: async () => {
        // get military profession match
        const response = await typeorm.q(`SELECT * FROM textMilitaryProfessionMatch WHERE deleted_date IS NULL ORDER BY id`, [])
        return response
    },
    updateMilitaryProfession: async (match: any) => {
        // update military profession match
        const where = [{ key: 'id', value: match.id }]
        const data = { text: match.text, counter: match.counter }
        await qb.update({ table: 'textMilitaryProfessionMatch', data, where })
    },
    removeMilitaryProfession: async (id: number) => {
        // remove military profession match
        const where = [{ key: 'id', value: id }]
        const data = { deleted_date: Date.now() }
        await qb.update({ table: 'textMilitaryProfessionMatch', data, where })

        // await qb.remove({ table: 'textMilitaryProfessionMatch', where: [{ key: 'id', value: id }] })
    },
    militaryProfession: async (matches: any) => {
        // m = { text, field, profession, counter }

        const response = []
        for (let i = 0; i < matches.length; i++) {
            const m = matches[i]

            // find if match exist
            const where = [
                { key: 'text', value: m.text },
                { key: 'field', value: m.field },
                { key: 'profession', value: m.profession }
            ]
            const match = await qb.read({ table: 'textMilitaryProfessionMatch', where })

            // update counter
            const rowCounter = match[0] ? match[0].counter : 0
            m.counter = parseInt(rowCounter) + parseInt(m.counter)

            // if exist, update. if not, add
            const row = match[0]
                ? await qb.update({ table: 'textMilitaryProfessionMatch', data: m, where: [{ key: 'id', value: match[0].id }] })
                : await qb.insert('textMilitaryProfessionMatch', m)

            response.push(row)
        }
        return response
    }
}

export default { run }
