const { qb, db } = require('../index')

async function run(type: string, matches: any) {
    // run function
    const response: any = await funcs[type](matches)
    return response
}

// set up the possible functions:
const funcs: any = {
    getPositions: async () => {
        // get positions match
        const response = await db.q(`SELECT * FROM similarPositionMatch WHERE deleted_at IS NULL ORDER BY id`)
        return response.rows
    },
    updatePosition: async (match: any) => {
        // update position match
        const where = [{ key: 'id', value: match.id }]
        const data = { similar_field: match.similar_field, similar_position: match.similar_position, counter: match.counter }
        await qb.update({ table: 'similarPositionMatch', data, where })
    },
    removePosition: async (id: number) => {
        // remove position match
        const where = [{ key: 'id', value: id }]
        const data = { deleted_at: Date.now() }
        await qb.update({ table: 'similarPositionMatch', data, where })

        // await qb.remove({ table: 'similarPositionMatch', where: [{ key: 'id', value: id }] })
    },
    position: async (matches: any) => {
        // m = { text, field, position, counter }
        const response = []
        for (let i = 0; i < matches.length; i++) {
            const m = matches[i]

            // find if match exist
            const where = [
                { key: 'similar_field', value: m.similar_field },
                { key: 'similar_position', value: m.similar_position },
                { key: 'field', value: m.field },
                { key: 'position', value: m.position }
            ]
            const match = await qb.read({ table: 'similarPositionMatch', where })

            // update counter
            const rowCounter = match[0] ? match[0].counter : 0
            m.counter = parseInt(rowCounter) + parseInt(m.counter)

            // if exist, update. if not, add
            const row = match[0]
                ? await qb.update({ table: 'similarPositionMatch', data: m, where: [{ key: 'id', value: match[0].id }] })
                : await qb.insert('similarPositionMatch', m)

            response.push(row)
        }
        return response
    },
    getDiplomas: async () => {
        // get diplomas match
        const response = await db.q(`SELECT * FROM similarDiplomaMatch WHERE deleted_at IS NULL ORDER BY id`)
        return response.rows
    },
    updateDiploma: async (match: any) => {
        // update diplomas match
        const where = [{ key: 'id', value: match.id }]
        const data = { similar_field: match.similar_field, similar_diploma: match.similar_diploma, counter: match.counter }
        await qb.update({ table: 'similarDiplomaMatch', data, where })
    },
    removeDiploma: async (id: number) => {
        // remove diplomas match
        const where = [{ key: 'id', value: id }]
        const data = { deleted_at: Date.now() }
        await qb.update({ table: 'similarDiplomaMatch', data, where })

        // await qb.remove({ table: 'similarDiplomaMatch', where: [{ key: 'id', value: id }] })
    },
    diploma: async (matches: any) => {
        // m = { text, field, diploma, counter }
        const response = []
        for (let i = 0; i < matches.length; i++) {
            const m = matches[i]

            // find if match exist
            const where = [
                { key: 'similar_field', value: m.similar_field },
                { key: 'similar_diploma', value: m.similar_diploma },
                { key: 'field', value: m.field },
                { key: 'diploma', value: m.diploma }
            ]
            const match = await qb.read({ table: 'similarDiplomaMatch', where })

            // update counter
            const rowCounter = match[0] ? match[0].counter : 0
            m.counter = parseInt(rowCounter) + parseInt(m.counter)

            // if exist, update. if not, add
            const row = match[0]
                ? await qb.update({ table: 'similarDiplomaMatch', data: m, where: [{ key: 'id', value: match[0].id }] })
                : await qb.insert('similarDiplomaMatch', m)

            response.push(row)
        }
        return response
    },
    getMilitaryProfession: async () => {
        // get military profession match
        const response = await db.q(`SELECT * FROM similarMilitaryProfessionMatch WHERE deleted_at IS NULL ORDER BY id`)
        return response.rows
    },
    updateMilitaryProfession: async (match: any) => {
        // update military profession match
        const where = [{ key: 'id', value: match.id }]
        const data = { similar_field: match.similar_field, similar_profession: match.similar_profession, counter: match.counter }
        await qb.update({ table: 'similarMilitaryProfessionMatch', data, where })
    },
    removeMilitaryProfession: async (id: number) => {
        // remove military profession match
        const where = [{ key: 'id', value: id }]
        const data = { deleted_at: Date.now() }
        await qb.update({ table: 'similarMilitaryProfessionMatch', data, where })

        // await qb.remove({ table: 'similarMilitaryProfessionMatch', where: [{ key: 'id', value: id }] })
    },
    militaryProfession: async (matches: any) => {
        // m = { text, field, profession, counter }

        const response = []
        for (let i = 0; i < matches.length; i++) {
            const m = matches[i]

            // find if match exist
            const where = [
                { key: 'similar_field', value: m.similar_field },
                { key: 'similar_profession', value: m.similar_profession },
                { key: 'field', value: m.field },
                { key: 'profession', value: m.profession }
            ]
            const match = await qb.read({ table: 'similarMilitaryProfessionMatch', where })

            // update counter
            const rowCounter = match[0] ? match[0].counter : 0
            m.counter = parseInt(rowCounter) + parseInt(m.counter)

            // if exist, update. if not, add
            const row = match[0]
                ? await qb.update({ table: 'similarMilitaryProfessionMatch', data: m, where: [{ key: 'id', value: match[0].id }] })
                : await qb.insert('similarMilitaryProfessionMatch', m)

            response.push(row)
        }
        return response
    }
}

export default { run }
