if (!process.env.NODE_ENV) process.env.NODE_ENV = 'local'

export default {
    apiPort: process.env.PORT,
    allowHelpAPI: process.env.debugMode === 'true',
    logAllRequests: process.env.debugMode === 'true',
    corsOrigin: process.env.corsOrigin,
    needSSL: process.env.needSSL === 'true',
    secretKey: process.env.secretKey
}
