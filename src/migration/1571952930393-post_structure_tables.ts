import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class postStructureTables1571952930393 implements MigrationInterface {


    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'posts',
            columns: [
                {
                    name: "id",
                    type: "bigint",
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment'
                },
                {
                    name: "candidate_id",
                    type: "bigint",
                    isNullable: true
                },
                {
                    name: "user_id",
                    type: "bigint",
                    isNullable: true
                },
                {
                    name: "is_laburo",
                    type: "boolean",
                    default: false,
                    isNullable: true
                },
                {
                    name: "content",
                    type: "text",
                    isNullable: true
                },
                {
                    name: "img",
                    type: "text",
                    isNullable: true
                },
                {
                    name: "filling",
                    type: 'varchar',
                    isNullable: true
                },
                {
                    name: "privacy",
                    type: 'varchar',
                    isNullable: true
                },
                {
                    name: "created_at",
                    type: "timestamp",
                    default: 'CURRENT_TIMESTAMP',
                    isNullable: true
                },
                {
                    name: "updated_at",
                    type: "timestamp",
                    default: 'CURRENT_TIMESTAMP',
                    isNullable: true
                },
                {
                    name: "deleted_at",
                    type: "timestamp",
                    isNullable: true
                }
            ]
        }), true)

        await queryRunner.createForeignKey('posts', new TableForeignKey({
            columnNames: ['candidate_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'candidates',
            onDelete: "CASCADE"
        }));

        await queryRunner.createForeignKey('posts', new TableForeignKey({
            columnNames: ['user_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'users',
            onDelete: "CASCADE"
        }));


        await queryRunner.createTable(new Table({
            name: 'post_like',
            columns: [
                {
                    name: "id",
                    type: "bigint",
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment'
                },
                {
                    name: "post_id",
                    type: "bigint",
                    isNullable: true
                },
                {
                    name: "candidate_id",
                    type: "bigint",
                    isNullable: true
                },
                {
                    name: "user_id",
                    type: "bigint",
                    isNullable: true
                },
                {
                    name: "is_laburo",
                    type: "boolean",
                    default: false,
                    isNullable: true
                },
                {
                    name: "created_at",
                    type: "timestamp",
                    default: 'CURRENT_TIMESTAMP',
                    isNullable: true
                }
            ]
        }), true)

        await queryRunner.createForeignKey('post_like', new TableForeignKey({
            columnNames: ['post_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'posts',
            onDelete: "CASCADE"
        }));

        await queryRunner.createForeignKey('post_like', new TableForeignKey({
            columnNames: ['candidate_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'candidates',
            onDelete: "CASCADE"
        }));

        await queryRunner.createForeignKey('post_like', new TableForeignKey({
            columnNames: ['user_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'users',
            onDelete: "CASCADE"
        }));


        await queryRunner.createTable(new Table({
            name: 'post_not_like',
            columns: [
                {
                    name: "id",
                    type: "bigint",
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment'
                },
                {
                    name: "post_id",
                    type: "bigint",
                    isNullable: true
                },
                {
                    name: "candidate_id",
                    type: "bigint",
                    isNullable: true
                },
                {
                    name: "user_id",
                    type: "bigint",
                    isNullable: true
                },
                {
                    name: "is_laburo",
                    type: "boolean",
                    default: false,
                    isNullable: true
                },
                {
                    name: "created_at",
                    type: "timestamp",
                    default: 'CURRENT_TIMESTAMP',
                    isNullable: true
                }
            ]
        }), true)

        await queryRunner.createForeignKey('post_not_like', new TableForeignKey({
            columnNames: ['post_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'posts',
            onDelete: "CASCADE"
        }));

        await queryRunner.createForeignKey('post_not_like', new TableForeignKey({
            columnNames: ['candidate_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'candidates',
            onDelete: "CASCADE"
        }));

        await queryRunner.createForeignKey('post_not_like', new TableForeignKey({
            columnNames: ['user_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'users',
            onDelete: "CASCADE"
        }));


        await queryRunner.createTable(new Table({
            name: 'comments',
            columns: [
                {
                    name: "id",
                    type: "bigint",
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment'
                },
                {
                    name: "post_id",
                    type: "bigint",
                    isNullable: true
                },
                {
                    name: "candidate_id",
                    type: "bigint",
                    isNullable: true
                },
                {
                    name: "user_id",
                    type: "bigint",
                    isNullable: true
                },
                {
                    name: "is_laburo",
                    type: "boolean",
                    default: false,
                    isNullable: true
                },
                {
                    name: "content",
                    type: "text",
                    isNullable: true
                },
                {
                    name: "img",
                    type: "text",
                    isNullable: true
                },
                {
                    name: "filling",
                    type: 'varchar',
                    isNullable: true
                },
                {
                    name: "privacy",
                    type: 'varchar',
                    isNullable: true
                },
                {
                    name: "created_at",
                    type: "timestamp",
                    default: 'CURRENT_TIMESTAMP',
                    isNullable: true
                },
                {
                    name: "updated_at",
                    type: "timestamp",
                    default: 'CURRENT_TIMESTAMP',
                    isNullable: true
                },
                {
                    name: "deleted_at",
                    type: "timestamp",
                    isNullable: true
                }
            ]
        }), true)

        await queryRunner.createForeignKey('comments', new TableForeignKey({
            columnNames: ['post_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'posts',
            onDelete: "CASCADE"
        }));

        await queryRunner.createForeignKey('comments', new TableForeignKey({
            columnNames: ['candidate_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'candidates',
            onDelete: "CASCADE"
        }));

        await queryRunner.createForeignKey('comments', new TableForeignKey({
            columnNames: ['user_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'users',
            onDelete: "CASCADE"
        }));


        await queryRunner.createTable(new Table({
            name: 'comment_like',
            columns: [
                {
                    name: "id",
                    type: "bigint",
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment'
                },
                {
                    name: "comment_id",
                    type: "bigint",
                    isNullable: true
                },
                {
                    name: "candidate_id",
                    type: "bigint",
                    isNullable: true
                },
                {
                    name: "user_id",
                    type: "bigint",
                    isNullable: true
                },
                {
                    name: "is_laburo",
                    type: "boolean",
                    default: false,
                    isNullable: true
                },
                {
                    name: "created_at",
                    type: "timestamp",
                    default: 'CURRENT_TIMESTAMP',
                    isNullable: true
                }
            ]
        }), true)

        await queryRunner.createForeignKey('comment_like', new TableForeignKey({
            columnNames: ['comment_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'comments',
            onDelete: "CASCADE"
        }));

        await queryRunner.createForeignKey('comment_like', new TableForeignKey({
            columnNames: ['candidate_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'candidates',
            onDelete: "CASCADE"
        }));

        await queryRunner.createForeignKey('comment_like', new TableForeignKey({
            columnNames: ['user_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'users',
            onDelete: "CASCADE"
        }));


        await queryRunner.createTable(new Table({
            name: 'comment_not_like',
            columns: [
                {
                    name: "id",
                    type: "bigint",
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment'
                },
                {
                    name: "comment_id",
                    type: "bigint",
                    isNullable: true
                },
                {
                    name: "candidate_id",
                    type: "bigint",
                    isNullable: true
                },
                {
                    name: "user_id",
                    type: "bigint",
                    isNullable: true
                },
                {
                    name: "is_laburo",
                    type: "boolean",
                    default: false,
                    isNullable: true
                },
                {
                    name: "created_at",
                    type: "timestamp",
                    default: 'CURRENT_TIMESTAMP',
                    isNullable: true
                }
            ]
        }), true)

        await queryRunner.createForeignKey('comment_not_like', new TableForeignKey({
            columnNames: ['comment_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'comments',
            onDelete: "CASCADE"
        }));

        await queryRunner.createForeignKey('comment_not_like', new TableForeignKey({
            columnNames: ['candidate_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'candidates',
            onDelete: "CASCADE"
        }));

        await queryRunner.createForeignKey('comment_not_like', new TableForeignKey({
            columnNames: ['user_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'users',
            onDelete: "CASCADE"
        }));


        await queryRunner.createTable(new Table({
            name: 'sub_comments',
            columns: [
                {
                    name: "id",
                    type: "bigint",
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment'
                },
                {
                    name: "comment_id",
                    type: "bigint",
                    isNullable: true
                },
                {
                    name: "candidate_id",
                    type: "bigint",
                    isNullable: true
                },
                {
                    name: "user_id",
                    type: "bigint",
                    isNullable: true
                },
                {
                    name: "is_laburo",
                    type: "boolean",
                    default: false,
                    isNullable: true
                },
                {
                    name: "content",
                    type: "text",
                    isNullable: true
                },
                {
                    name: "filling",
                    type: 'varchar',
                    isNullable: true
                },
                {
                    name: "privacy",
                    type: 'varchar',
                    isNullable: true
                },
                {
                    name: "created_at",
                    type: "timestamp",
                    default: 'CURRENT_TIMESTAMP',
                    isNullable: true
                },
                {
                    name: "updated_at",
                    type: "timestamp",
                    default: 'CURRENT_TIMESTAMP',
                    isNullable: true
                },
                {
                    name: "deleted_at",
                    type: "timestamp",
                    isNullable: true
                }
            ]
        }), true)

        await queryRunner.createForeignKey('sub_comments', new TableForeignKey({
            columnNames: ['comment_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'comments',
            onDelete: "CASCADE"
        }));

        await queryRunner.createForeignKey('sub_comments', new TableForeignKey({
            columnNames: ['candidate_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'candidates',
            onDelete: "CASCADE"
        }));

        await queryRunner.createForeignKey('sub_comments', new TableForeignKey({
            columnNames: ['user_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'users',
            onDelete: "CASCADE"
        }));



        await queryRunner.createTable(new Table({
            name: 'sub_comment_like',
            columns: [
                {
                    name: "id",
                    type: "bigint",
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment'
                },
                {
                    name: "sub_comment_id",
                    type: "bigint",
                    isNullable: true
                },
                {
                    name: "candidate_id",
                    type: "bigint",
                    isNullable: true
                },
                {
                    name: "user_id",
                    type: "bigint",
                    isNullable: true
                },
                {
                    name: "is_laburo",
                    type: "boolean",
                    default: false,
                    isNullable: true
                },
                {
                    name: "created_at",
                    type: "timestamp",
                    default: 'CURRENT_TIMESTAMP',
                    isNullable: true
                }
            ]
        }), true)

        await queryRunner.createForeignKey('sub_comment_like', new TableForeignKey({
            columnNames: ['sub_comment_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'sub_comments',
            onDelete: "CASCADE"
        }));

        await queryRunner.createForeignKey('sub_comment_like', new TableForeignKey({
            columnNames: ['candidate_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'candidates',
            onDelete: "CASCADE"
        }));

        await queryRunner.createForeignKey('sub_comment_like', new TableForeignKey({
            columnNames: ['user_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'users',
            onDelete: "CASCADE"
        }));


        await queryRunner.createTable(new Table({
            name: 'sub_comment_not_like',
            columns: [
                {
                    name: "id",
                    type: "bigint",
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment'
                },
                {
                    name: "sub_comment_id",
                    type: "bigint",
                    isNullable: true
                },
                {
                    name: "candidate_id",
                    type: "bigint",
                    isNullable: true
                },
                {
                    name: "user_id",
                    type: "bigint",
                    isNullable: true
                },
                {
                    name: "is_laburo",
                    type: "boolean",
                    default: false,
                    isNullable: true
                },
                {
                    name: "created_at",
                    type: "timestamp",
                    default: 'CURRENT_TIMESTAMP',
                    isNullable: true
                }
            ]
        }), true)

        await queryRunner.createForeignKey('sub_comment_not_like', new TableForeignKey({
            columnNames: ['sub_comment_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'sub_comments',
            onDelete: "CASCADE"
        }));

        await queryRunner.createForeignKey('sub_comment_not_like', new TableForeignKey({
            columnNames: ['candidate_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'candidates',
            onDelete: "CASCADE"
        }));

        await queryRunner.createForeignKey('sub_comment_not_like', new TableForeignKey({
            columnNames: ['user_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'users',
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable('posts')
        if (table) {
            const foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf('candidate_id') !== -1)
            if (foreignKey) { await queryRunner.dropForeignKey('posts', foreignKey) }

            const foreignKey2 = table.foreignKeys.find(fk => fk.columnNames.indexOf('user_id') !== -1)
            if (foreignKey2) { await queryRunner.dropForeignKey('posts', foreignKey2) }
        }
        await queryRunner.dropTable('posts')


        const table2 = await queryRunner.getTable('post_like')
        if (table2) {
            const foreignKey = table2.foreignKeys.find(fk => fk.columnNames.indexOf('candidate_id') !== -1)
            if (foreignKey) { await queryRunner.dropForeignKey('post_like', foreignKey) }

            const foreignKey2 = table2.foreignKeys.find(fk => fk.columnNames.indexOf('user_id') !== -1)
            if (foreignKey2) { await queryRunner.dropForeignKey('post_like', foreignKey2) }

            const foreignKey3 = table2.foreignKeys.find(fk => fk.columnNames.indexOf('post_id') !== -1)
            if (foreignKey3) { await queryRunner.dropForeignKey('post_like', foreignKey3) }
        }
        await queryRunner.dropTable('post_like')


        const table3 = await queryRunner.getTable('post_not_like')
        if (table3) {
            const foreignKey = table3.foreignKeys.find(fk => fk.columnNames.indexOf('candidate_id') !== -1)
            if (foreignKey) { await queryRunner.dropForeignKey('post_not_like', foreignKey) }

            const foreignKey2 = table3.foreignKeys.find(fk => fk.columnNames.indexOf('user_id') !== -1)
            if (foreignKey2) { await queryRunner.dropForeignKey('post_not_like', foreignKey2) }

            const foreignKey3 = table3.foreignKeys.find(fk => fk.columnNames.indexOf('post_id') !== -1)
            if (foreignKey3) { await queryRunner.dropForeignKey('post_not_like', foreignKey3) }
        }
        await queryRunner.dropTable('post_not_like')


        const table4 = await queryRunner.getTable('comments')
        if (table4) {
            const foreignKey = table4.foreignKeys.find(fk => fk.columnNames.indexOf('candidate_id') !== -1)
            if (foreignKey) { await queryRunner.dropForeignKey('comments', foreignKey) }

            const foreignKey2 = table4.foreignKeys.find(fk => fk.columnNames.indexOf('user_id') !== -1)
            if (foreignKey2) { await queryRunner.dropForeignKey('comments', foreignKey2) }

            const foreignKey3 = table4.foreignKeys.find(fk => fk.columnNames.indexOf('post_id') !== -1)
            if (foreignKey3) { await queryRunner.dropForeignKey('comments', foreignKey3) }
        }
        await queryRunner.dropTable('comments')


        const table5 = await queryRunner.getTable('comment_like')
        if (table5) {
            const foreignKey = table5.foreignKeys.find(fk => fk.columnNames.indexOf('candidate_id') !== -1)
            if (foreignKey) { await queryRunner.dropForeignKey('comment_like', foreignKey) }

            const foreignKey2 = table5.foreignKeys.find(fk => fk.columnNames.indexOf('user_id') !== -1)
            if (foreignKey2) { await queryRunner.dropForeignKey('comment_like', foreignKey2) }

            const foreignKey3 = table5.foreignKeys.find(fk => fk.columnNames.indexOf('comment_id') !== -1)
            if (foreignKey3) { await queryRunner.dropForeignKey('comment_like', foreignKey3) }
        }
        await queryRunner.dropTable('comment_like')

        const table6 = await queryRunner.getTable('comment_not_like')
        if (table6) {
            const foreignKey = table6.foreignKeys.find(fk => fk.columnNames.indexOf('candidate_id') !== -1)
            if (foreignKey) { await queryRunner.dropForeignKey('comment_not_like', foreignKey) }

            const foreignKey2 = table6.foreignKeys.find(fk => fk.columnNames.indexOf('user_id') !== -1)
            if (foreignKey2) { await queryRunner.dropForeignKey('comment_not_like', foreignKey2) }

            const foreignKey3 = table6.foreignKeys.find(fk => fk.columnNames.indexOf('comment_id') !== -1)
            if (foreignKey3) { await queryRunner.dropForeignKey('comment_not_like', foreignKey3) }
        }
        await queryRunner.dropTable('comment_not_like')


        const table7 = await queryRunner.getTable('sub_comments')
        if (table7) {
            const foreignKey = table7.foreignKeys.find(fk => fk.columnNames.indexOf('candidate_id') !== -1)
            if (foreignKey) { await queryRunner.dropForeignKey('sub_comments', foreignKey) }

            const foreignKey2 = table7.foreignKeys.find(fk => fk.columnNames.indexOf('user_id') !== -1)
            if (foreignKey2) { await queryRunner.dropForeignKey('sub_comments', foreignKey2) }

            const foreignKey3 = table7.foreignKeys.find(fk => fk.columnNames.indexOf('comment_id') !== -1)
            if (foreignKey3) { await queryRunner.dropForeignKey('sub_comments', foreignKey3) }
        }
        await queryRunner.dropTable('sub_comments')


        const table8 = await queryRunner.getTable('sub_comment_like')
        if (table8) {
            const foreignKey = table8.foreignKeys.find(fk => fk.columnNames.indexOf('candidate_id') !== -1)
            if (foreignKey) { await queryRunner.dropForeignKey('sub_comment_like', foreignKey) }

            const foreignKey2 = table8.foreignKeys.find(fk => fk.columnNames.indexOf('user_id') !== -1)
            if (foreignKey2) { await queryRunner.dropForeignKey('sub_comment_like', foreignKey2) }

            const foreignKey3 = table8.foreignKeys.find(fk => fk.columnNames.indexOf('sub_comment_id') !== -1)
            if (foreignKey3) { await queryRunner.dropForeignKey('sub_comment_like', foreignKey3) }
        }
        await queryRunner.dropTable('sub_comment_like')


        const table9 = await queryRunner.getTable('sub_comment_not_like')
        if (table9) {
            const foreignKey = table9.foreignKeys.find(fk => fk.columnNames.indexOf('candidate_id') !== -1)
            if (foreignKey) { await queryRunner.dropForeignKey('sub_comment_not_like', foreignKey) }

            const foreignKey2 = table9.foreignKeys.find(fk => fk.columnNames.indexOf('user_id') !== -1)
            if (foreignKey2) { await queryRunner.dropForeignKey('sub_comment_not_like', foreignKey2) }

            const foreignKey3 = table9.foreignKeys.find(fk => fk.columnNames.indexOf('sub_comment_id') !== -1)
            if (foreignKey3) { await queryRunner.dropForeignKey('sub_comment_not_like', foreignKey3) }
        }
        await queryRunner.dropTable('sub_comment_not_like')
    }
}
