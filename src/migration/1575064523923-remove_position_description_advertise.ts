import {MigrationInterface, QueryRunner} from "typeorm";

export class removePositionDescriptionAdvertise1575064523923 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn('job_advertise', 'position_description')
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
