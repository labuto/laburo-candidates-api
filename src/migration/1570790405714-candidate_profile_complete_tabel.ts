import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class candidateProfileCompleteTabel1570790405714 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'candidate_profile_complete',
            columns: [
                {
                    name: "id",
                    type: "bigint",
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment'
                },
                {
                    name: "candidate_id",
                    type: "bigint",
                },
                {
                    name: "general",
                    type: "boolean",
                    default: false
                },
                {
                    name: "experience",
                    type: "boolean",
                    default: false
                },
                {
                    name: "education",
                    type: "boolean",
                    default: false
                },
                {
                    name: "military",
                    type: "boolean",
                    default: false
                },
                {
                    name: "skills",
                    type: "boolean",
                    default: false
                }
            ]
        }), true)

        await queryRunner.createForeignKey('candidate_profile_complete', new TableForeignKey({
            columnNames: ['candidate_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'candidates',
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable('candidate_profile_complete')
        if (table) {
            const foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf('candidate_id') !== -1)
            if (foreignKey) {
                await queryRunner.dropForeignKey('candidate_profile_complete', foreignKey)
            }
        }
        await queryRunner.dropTable('candidate_profile_complete')
    }

}
