import {MigrationInterface, QueryRunner} from "typeorm";
import {Candidate} from '../entity/Candidate'
import {CandidateProfileComplete} from '../entity/Candidate/CandidateProfileComplete'

export class addCandidatesToProfileComplete1570791333408 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        try {
            const candidates = await Candidate.find()
            for (let i = 0; i < candidates.length; i++) {
                await CandidateProfileComplete.create({ candidate_id: candidates[i].id }).save()
            }
        } catch (e) { console.error(e) }
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
