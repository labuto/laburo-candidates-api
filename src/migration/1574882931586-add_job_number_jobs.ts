import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class addJobNumberJobs1574882931586 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumn('jobs', new TableColumn({
            name: 'job_number',
            type: 'varchar(10)',
            isNullable: true
        }))
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn('jobs', 'job_number')
    }

}
