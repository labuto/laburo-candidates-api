import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class addCandidateJobShare1570536240869 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable('candidateshare')
        if (table) {
            const foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf('candidate_id') !== -1)
            if (foreignKey) {
                await queryRunner.dropForeignKey('candidateshare', foreignKey)
            }
        }
        await queryRunner.dropTable('candidateshare')

        await queryRunner.createTable(new Table({
            name: 'candidate_job_share',
            columns: [
                {
                    name: "id",
                    type: "bigint",
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment'
                },
                {
                    name: "candidate_id",
                    type: "bigint",
                },
                {
                    name: "job_id",
                    type: "bigint",
                },
                {
                    name: "share_email",
                    type: "varchar(255)",
                },
                {
                    name: "approve",
                    type: "boolean",
                    default: false
                },
                {
                    name: "created_at",
                    type: "timestamp",
                    default: 'CURRENT_TIMESTAMP'
                },
                {
                    name: "updated_at",
                    type: "timestamp",
                    default: 'CURRENT_TIMESTAMP'
                },
                {
                    name: "deleted_at",
                    type: "timestamp"
                }
            ]
        }), true)

        await queryRunner.createForeignKey('candidate_job_share', new TableForeignKey({
            columnNames: ['candidate_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'candidates',
            onDelete: "CASCADE"
        }));

        await queryRunner.createForeignKey('candidate_job_share', new TableForeignKey({
            columnNames: ['job_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'jobs',
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
