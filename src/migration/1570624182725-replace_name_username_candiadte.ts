import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class replaceNameUsernameCandiadte1570624182725 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "candidates" RENAME COLUMN "username" TO "full_name"`);

        await queryRunner.dropColumn('candidate_general', 'full_name')
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "candidates" RENAME COLUMN "full_name" TO "username"`);
    }

}
