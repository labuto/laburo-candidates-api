import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class addOtherJobAdvertise1575017766862 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumn('job_advertise', new TableColumn({
            name: 'customize',
            type: 'json',
            isNullable: true
        }))
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn('job_advertise', 'customize')
    }

}
