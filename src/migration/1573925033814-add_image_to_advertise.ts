import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class addImageToAdvertise1573925033814 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumn('job_advertise', new TableColumn({
            name: 'image',
            type: 'json',
            isNullable: true,
            default: `'{}'`
        }))
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn('job_advertise', 'image')

    }

}
