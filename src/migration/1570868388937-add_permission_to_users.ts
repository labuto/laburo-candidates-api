import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class addPermissionToUsers1570868388937 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumn('users', new TableColumn({
            name: 'permission',
            type: 'int',
            isNullable: true
        }))
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn('users', 'permission')
    }

}
