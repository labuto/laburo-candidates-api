import {MigrationInterface, QueryRunner, Table, TableIndex, TableForeignKey} from "typeorm";

export class candidateHobbieTable1569079593045 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'candidate_hobbies',
            columns: [
                {
                    name: "id",
                    type: "bigint",
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment'
                },
                {
                    name: "candidate_id",
                    type: "bigint",
                },
                {
                    name: "value",
                    type: "text",
                }
            ]
        }), true)

        await queryRunner.createForeignKey('candidate_hobbies', new TableForeignKey({
            columnNames: ['candidate_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'candidates',
            onDelete: "CASCADE"
        }));

        await queryRunner.createTable(new Table({
            name: 'candidate_skills',
            columns: [
                {
                    name: "id",
                    type: "bigint",
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment'
                },
                {
                    name: "candidate_id",
                    type: "bigint",
                },
                {
                    name: "value",
                    type: "text",
                }
            ]
        }), true)

        await queryRunner.createForeignKey('candidate_skills', new TableForeignKey({
            columnNames: ['candidate_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'candidates',
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable('candidate_hobbies')
        if (table) {
            const foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf('candidate_id') !== -1)
            if (foreignKey) {
                await queryRunner.dropForeignKey('candidate_hobbies', foreignKey)
            }
        }
        await queryRunner.dropTable('candidate_hobbies')

        const table2 = await queryRunner.getTable('candidate_skills')
        if (table2) {
            const foreignKey2 = table2.foreignKeys.find(fk => fk.columnNames.indexOf('candidate_id') !== -1)
            if (foreignKey2) {
                await queryRunner.dropForeignKey('candidate_skills', foreignKey2)
            }
        }
        await queryRunner.dropTable('candidate_skills')
    }

}
