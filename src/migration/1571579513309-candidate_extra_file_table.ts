import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class candidateExtraFileTable1571579513309 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'candidate_extra_files',
            columns: [
                {
                    name: "id",
                    type: "bigint",
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'increment'
                },
                {
                    name: "candidate_id",
                    type: "bigint",
                },
                {
                    name: "file_name",
                    type: "text",
                },
                {
                    name: "file_link",
                    type: "text",
                }
            ]
        }), true)

        await queryRunner.createForeignKey('candidate_extra_files', new TableForeignKey({
            columnNames: ['candidate_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'candidates',
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        const table = await queryRunner.getTable('candidate_extra_file')
        if (table) {
            const foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf('candidate_id') !== -1)
            if (foreignKey) {
                await queryRunner.dropForeignKey('candidate_extra_file', foreignKey)
            }
        }
        await queryRunner.dropTable('candidate_extra_file')
    }

}
