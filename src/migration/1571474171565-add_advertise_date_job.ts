import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class addAdvertiseDateJob1571474171565 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.addColumn('jobs', new TableColumn({
            name: 'advertise_at',
            type: 'timestamp',
            default: 'CURRENT_TIMESTAMP',
            isNullable: true
        }))

        await queryRunner.addColumn('jobs', new TableColumn({
            name: 'advertise_is_initial',
            type: 'boolean',
            default: true,
            isNullable: true
        }))
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropColumn('jobs', 'advertise_at')
        await queryRunner.dropColumn('jobs', 'advertise_is_initial')
    }
}
