import { Request, Response, NextFunction } from 'express'

module.exports = (req: Request, res: Response, next: NextFunction) => {
    req.user && req.isAuthenticated()
        ? next()
        : res.status(403).send('no permission, login first')
}
