import { Request, Response, NextFunction } from 'express'

// Helper function to wrap async middleware with proper error handling
module.exports = (fn: any) =>
    (req: Request, res: Response, next: NextFunction) => {
        Promise.resolve(fn(req, res, next))
            .catch(next)
    }