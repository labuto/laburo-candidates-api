import { Request, Response, NextFunction } from 'express'
import config from '../serverConfig'
const secretKey: string | undefined = config.secretKey
const { cryptoJs } = require('../services')

module.exports = (req: Request, res: Response, next: NextFunction) => {
  const publicKey: string | undefined = req.headers.authorization
  const key = cryptoJs.myDecrypt(secretKey)
  key === publicKey
    ? next()
    : res.status(405).send('not authorize')
}
