import express, { Application, Router } from 'express'
import path from 'path'
import bodyParser from 'body-parser'
import config from './serverConfig'
import fs from 'fs'

import http from 'http'
import https from 'https'

const cors = require('cors')
const fileUpload = require('express-fileupload')

import socketIo from './config/socketIo'

export default class Server {
  private app: Application = express()
  private server: any
  private readonly api: any

  constructor (api: any) {
    this.api = api
  }

  async init () {
    this.createServer()

    // if (config.logAllRequests) { this.app.use(logReq) }

    this.app.use(cors({origin: config.corsOrigin, credentials: true}))

    this.app.use(bodyParser.json({limit: '500mb'}))
    this.app.use(bodyParser.urlencoded({ extended: false }))

    this.app.use(fileUpload())

    this.expressStatic()

    this.createRouter()

    socketIo.init(this.server)

    await this.setExpressServer()
  }

  private createServer () {
    if (!config.needSSL) {
      this.server = http.createServer(this.app)
    } else {
      const key: string = fs.readFileSync(path.join(__dirname, 'secret/laburo.zone.key'), 'utf8').toString()
      const cert: string = fs.readFileSync(path.join(__dirname, 'secret/laburo.zone.crt'), 'utf8').toString()
      this.server = https.createServer({ key, cert }, this.app)

      http.createServer((req, res) => {
        res.writeHead(301, { 'Location': 'https://' + req.headers['host'] + req.url })
        res.end()
      }).listen(80)
    }
  }

  private expressStatic () {
    this.app.use(express.static(path.join(__dirname, '/../dist/spa/')))
    this.app.use('/.well-known/pki-validation', express.static(path.join(__dirname, '.well-known/pki-validation')))
    this.app.use('/public',express.static(path.join(__dirname, '/../public')))
  }

  private createRouter () {
    const apiRouter: Router = express.Router()

    // Create all routes edges with '/routes' prefix
    this.api(apiRouter)

    if (config.allowHelpAPI) {
      const apiEdgeDefinitions: Array<object> = []
      apiRouter.stack.map(x => x.route).filter(r => r)
        .forEach(r => {
          Object.keys(r.methods).forEach(m => {
            apiEdgeDefinitions.push({path: r.path, method: m})
          })
        })

      apiRouter.get('/help', (req, res) => {
        res.send(apiEdgeDefinitions)
      })
    }
    this.app.use('/api', apiRouter)

    // Fallback to allow vue router work it's charm
    this.app.get('*', (req, res) => { res.sendFile(path.resolve(path.join(__dirname, '/../dist/spa/index.html'))) })
  }

  private async setExpressServer () {
    await new Promise((resolve) => {
      const port: number = config.apiPort
      this.server.listen(port, () => {
        console.log(`Server is listening on port ${port}`)
        const shutdown = () => {
          console.log('Server closed')
          try {
            this.server.close()
          } catch (ex) {}
          process.exit()
        }
        process.once('SIGINT', shutdown).once('SIGTERM', shutdown)
        resolve()
      })
    })
  }
}
