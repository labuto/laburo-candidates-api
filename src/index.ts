
process.on('unhandledRejection', (reason, p) => {
    console.error('Unhandled Rejection at: Promise', p, 'reason:', reason)
})

require('dotenv').config()
import api from './routes'
import Server from './server'
const server = new Server(api)

import typeOrm from './services/typeOrm'

;(async () => {
    await typeOrm.init()
    await server.init()
})()
